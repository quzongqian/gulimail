package com.qzq.gulimall.order.feign;


import com.qzq.gulimall.order.vo.MemberAddressVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient("gulimall-member")
public interface MemberFeignServer {

    @GetMapping("/member/memberreceiveaddress/{id}/addressList")
    List<MemberAddressVo> addressList(@PathVariable("id")Long id);
}
