package com.qzq.gulimall.order.web;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qzq.gulimall.order.entity.OrderEntity;
import com.qzq.gulimall.order.service.OrderService;
import com.qzq.gulimall.order.vo.OrderConfirmVo;
import com.qzq.gulimall.order.vo.OrderSubmitVo;
import com.qzq.gulimall.order.vo.SubmitOrderResponseVo;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import java.nio.charset.StandardCharsets;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

@Controller
public class OrderWebController {

    @Resource
    private OrderService orderService;

    @Resource
    private RabbitTemplate rabbitTemplate;


    @ResponseBody
    @GetMapping("/test/createOrder")
    public String testCreateOrder(){

        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setOrderSn(UUID.randomUUID().toString());
        rabbitTemplate.convertSendAndReceive("order-event-exchange",
                "order.create.order", orderEntity);

        return "ok";
    }


    @GetMapping("/toTrade")
    public String toTrade(Model model) throws ExecutionException, InterruptedException {

        OrderConfirmVo orderConfirmVo = orderService.orderConfirm();
        model.addAttribute("orderConfirmData", orderConfirmVo);
        return "confirm.html";
    }

    @PostMapping("/submitOrder")
    public String submitOrder(OrderSubmitVo orderSubmitVo, Model model, RedirectAttributes redirectAttributes){

        SubmitOrderResponseVo responseVo = orderService.submitOrder(orderSubmitVo);

        if (responseVo.getCode() == 0){
            model.addAttribute("submitOrderResp",responseVo);
            //成功：来到支付页
            return "pay";
        } else {
            //失败：返回重新确认订单
            String msg = "下单失败";
            switch (responseVo.getCode()){
                case 1 : msg += "订单信息过期，请再次刷新提交"; break;
                case 2 : msg += "订单商品价格发生变化，请确认后重新提交"; break;
                case 3 : msg += "库存锁定失败，商品库存不足"; break;
            }
            redirectAttributes.addFlashAttribute("msg", msg);
            return "redirect://order.gulimall.com/toTrade";
        }

    }
}
