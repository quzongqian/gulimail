package com.qzq.gulimall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qzq.common.to.mq.SeckillOrderTo;
import com.qzq.common.utils.PageUtils;
import com.qzq.gulimall.order.entity.OrderEntity;
import com.qzq.gulimall.order.vo.*;

import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * 订单
 *
 * @author quzongqian
 * @email 2959720739@qq.com
 * @date 2023-03-19 20:41:03
 */
public interface OrderService extends IService<OrderEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 订单确认页需要的数据
     * @return
     */
    OrderConfirmVo orderConfirm() throws ExecutionException, InterruptedException;

    SubmitOrderResponseVo submitOrder (OrderSubmitVo orderSubmitVo);

    OrderEntity getByStatusByOrderSn(String orderSn);

    void closeLockStock(OrderEntity order);

    PayVo getOrderPay(String orderSn);

    PageUtils queryPageWithItem(Map<String, Object> params);

    String handlePayResult(PayAsyncVo vo);

    void createSeckillOrder(SeckillOrderTo seckillOrder);
}

