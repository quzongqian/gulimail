//package com.qzq.gulimall.order.config;
//
//import com.zaxxer.hikari.HikariDataSource;
//import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.util.StringUtils;
//
//import javax.annotation.Resource;
//import javax.sql.DataSource;
//
//@Configuration
//public class gulimallSeataConfig {
//
//    @Resource
//    private DataSourceProperties dataSourceProperties;
//
//    @Bean
//    public DataSource dataSource(DataSourceProperties dataSourceProperties){
//        //properties.initializeDataSourceBuilder().type(type).build()
//        HikariDataSource build = dataSourceProperties.initializeDataSourceBuilder().type(HikariDataSource.class).build();
//        if (StringUtils.hasText(dataSourceProperties.getName())) {
//            build.setPoolName(dataSourceProperties.getName());
//        }
//
//        return new DataSourceProxy(build);
//    }
//
//}
