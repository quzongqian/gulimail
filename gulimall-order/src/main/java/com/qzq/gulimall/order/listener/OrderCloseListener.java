package com.qzq.gulimall.order.listener;

import com.qzq.gulimall.order.entity.OrderEntity;
import com.qzq.gulimall.order.service.OrderService;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;


@Service
@RabbitListener(queues = "order.release.order.queue")
public class OrderCloseListener {

    @Resource
    private OrderService orderService;

    @RabbitHandler
    public void OrderReleaseOrder(OrderEntity order, Channel channel, Message message) throws IOException {
        System.out.println("收到消息 准备关闭订单 " + order.getOrderSn());
        //延时队列过后 改订单没有下单，所以开单 把订单的状态改为 4 已取消 让 后面库存延迟队列消息解锁库存
        try {
            orderService.closeLockStock(order);
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        } catch (Exception e) {
            channel.basicReject(message.getMessageProperties().getDeliveryTag(), true);
        }

    }
}
