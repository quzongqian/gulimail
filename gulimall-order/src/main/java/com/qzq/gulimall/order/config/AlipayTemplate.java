package com.qzq.gulimall.order.config;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.qzq.gulimall.order.vo.PayVo;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "alipay")
@Component
@Data
public class AlipayTemplate {

    //在支付宝创建的应用的id
    private   String app_id = "2021000122686837";

    // 商户私钥，您的PKCS8格式RSA2私钥
    private  String merchant_private_key = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDOd9FuMIio2Xfa62Ie0jUm1wSWX5IIw62Rf+04sqF+66PS6T9iFIzN2s1JEwc8p+E1+29/IN9FRc7us1BPNQOfu8ztyoOgnji/1f0euDmrHSwLY6J5KbKaVHSrp2+prnTGg66Tm27+SKz/2Bo1AH7I2uzzKfGVqEwFWLQH2LIuPeU7AL8HyZ65W+k1+kaCiB5x5L4q0MVnKb/l3JIAkQeUL/TEOQj6d7FEtDUnp6Nzn5IMuR3bON7xPgVdxdhgvg2uLfZfGaV5aHKvSOT/tX9J9392gVOyzW/TRb2xEpa2wok2g4UW+GUNZkFKHSKAa4M+YQmCoE6TWOntKeT434IdAgMBAAECggEAf7U7NJYOoapJ6X545amOOOT8sGqS8JrgjEP9nnlXTSNB5N5jVy5w3Y++NLnxWwJYWkSY9aCGmdmRIrKFNUkMYcwO+PKvsjTL01TwSQUYno/fw3ewcpNpJu6gb+CUP/dpwbSH0kNtRxn68BFiPHB++4v2zn2eMby0sQXp7YSKyfBgNA2ZjUA2ad6lB4UB2+pVwAqOa3QxFjWrIVSQNrZokVUetduWdrbj8tuDXpHQOkaTvzwsTwFPC5ZSq/HCxqmBH6xcM/1T17gvJ1d79Q+4Avka3LtSW+4YgjVUdaG+v5ctQDE+EoPUq7z7NAXi0qHFDlk5EbveXCKPg0Uvlmv2/QKBgQDzFdxip2f/u4UHHkuvz7j8iPLYqWrns+CbhTuDGTPL5uVXio7ysqo7QhrtGCewF92V0bHMj0zf9eYm1fF097LFAQUMIIB3LuzNJ0T2hMg3AjQng5hXyIhobsn97sa9GnUXEJXErtq71ooaodObYxxA3Y3p+Wcf7zhqD/hNSo4UxwKBgQDZb+81SUaDDw+EyLw26oIThx7IO/8Bs2t9CWyZv5fktjELJ4Z0XNlV8Wy9I0phQVrlQC0NysWRTlq9tW3ZkltBPgWE3TbfdTFROeRfXhmW5oIJ9pjWR/jToY11gt1tEZuxRDUCxl1i/eYd5exVyHX+FBFZyBRBqznLTOYyLczF+wKBgGE2b+FgM34ZHvUez4Pq3X7Ywc2VyzQIQp3PZWJjVMyJdVRSDCLRArUaFayVyWFp2erNZn55HHRAYZ9MVfjCClMwujUEVnvTX04YE0NHDM666eKXhPguMScaj1sm/4GmsAbeUhbLGpyeCrLl3rs4opbqQ5O7cttZjsUTx+gZFcMHAoGAaaojrNl3Eql2+SgzjtxXafX314aQXds7GQBDblPVFKgVHT6P8GxOkZ1JIoDL3IdODKorOtCXQg25uwc34stk3UeuluqJh/4DgV2IxW+NL+/EseLMUxWWrgGMJNb7k81Cw9pJiJetOjX4Zf8d4nIQJPDLnb3FWkRDSbCNDUWEhZECgYEAgJCR4yIBTIEoqpsDuwmCxoEoyY7yoP8n5BO8V+s57Q6W6CiNli94ip+MTtLADrNbeFuuyBBpcWkmLrpMWXinyRgcYPYgj/p1Ck9mlTSGMxOCNeA4uV+tAGMwdbuz54E8X6uvQtDYYvKJWVoNmty+MmWnvvBWiOXiNudjF6HWos8=";
    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    private  String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqtLTcOem4FpLWAyZwFEpoAhJkEqStZFdtWzcYlVhRcAdZbOBzrUVk0G9XF8GWRMzoz6KfsjqWOvMA7kVaziXS9a3SIhORjMbEnCxpdMzshDh3IXSY7AMzAQdJHi00ZkhbWG73SIyLD9WThyeQHuB8gb+hFh9w/18XPUsFtPnNw2fTRMpbGvwfbUXsvnDfLwohvphgxNFbPLYZfP+nRWlkIRPhRZkgahVBh95eulgbjhGD0mFF6fSgMQ9uaG3/tu4bb6icWYchWOF27rslEbfpn+QxoVedbYLczMJkT+gJIhgR6LL9tew68BCIx0b6NRPCogsLuY9epVIlOzJP+Ll0QIDAQAB";
    // 服务器[异步通知]页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    // 支付宝会悄悄的给我们发送一个请求，告诉我们支付成功的信息
    private  String notify_url ="http://ys899g.natappfree.cc/payed/notify";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    //同步通知，支付成功，一般跳转到成功页
    private  String return_url = "http://member.gulimall.com/memberOrder.html";

    // 签名方式
    private  String sign_type = "RSA2";

    // 字符编码格式
    private  String charset = "utf-8";

    private String timeout = "30m";

    // 支付宝网关； https://openapi.alipaydev.com/gateway.do
    private  String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    public  String pay(PayVo vo) throws AlipayApiException {

        //AlipayClient alipayClient = new DefaultAlipayClient(AlipayTemplate.gatewayUrl, AlipayTemplate.app_id, AlipayTemplate.merchant_private_key, "json", AlipayTemplate.charset, AlipayTemplate.alipay_public_key, AlipayTemplate.sign_type);
        //1、根据支付宝的配置生成一个支付客户端
        AlipayClient alipayClient = new DefaultAlipayClient(gatewayUrl,
                app_id, merchant_private_key, "json",
                charset, alipay_public_key, sign_type);

        //2、创建一个支付请求 //设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(return_url);
        alipayRequest.setNotifyUrl(notify_url);

        //商户订单号，商户网站订单系统中唯一订单号，必填
        String out_trade_no = vo.getOut_trade_no();
        //付款金额，必填
        String total_amount = vo.getTotal_amount();
        //订单名称，必填
        String subject = vo.getSubject();
        //商品描述，可空
        String body = vo.getBody();

        alipayRequest.setBizContent("{\"out_trade_no\":\""+ out_trade_no +"\","
                + "\"total_amount\":\""+ total_amount +"\","
                + "\"subject\":\""+ subject +"\","
                + "\"body\":\""+ body +"\","
                + "\"timeout_express\":\""+timeout+"\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

        String result = alipayClient.pageExecute(alipayRequest).getBody();

        //会收到支付宝的响应，响应的是一个页面，只要浏览器显示这个页面，就会自动来到支付宝的收银台页面
        System.out.println("支付宝的响应："+result);

        return result;

    }
}
