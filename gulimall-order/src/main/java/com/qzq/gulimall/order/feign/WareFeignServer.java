package com.qzq.gulimall.order.feign;

import com.qzq.common.to.SkuHasStockTo;
import com.qzq.common.utils.R;
import com.qzq.gulimall.order.vo.WareSkuLockVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient("gulimall-ware")
public interface WareFeignServer {


    @PostMapping("/ware/waresku/hasStock")
    R hasStock(@RequestBody List<Long> SkuId);


    @GetMapping("/ware/wareinfo/fare")
    R getFare(@RequestParam("addrId") Long id);

    @PostMapping("/ware/waresku/lock/order")
    R orderLockStock(@RequestBody WareSkuLockVo vo);
}
