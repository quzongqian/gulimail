package com.qzq.gulimall.coupon.dao;

import com.qzq.gulimall.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 * 
 * @author quzongqian
 * @email 2959720739@qq.com
 * @date 2023-03-19 19:42:24
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {
	
}
