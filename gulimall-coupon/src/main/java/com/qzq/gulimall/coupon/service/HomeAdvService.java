package com.qzq.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qzq.common.utils.PageUtils;
import com.qzq.gulimall.coupon.entity.HomeAdvEntity;

import java.util.Map;

/**
 * 首页轮播广告
 *
 * @author quzongqian
 * @email 2959720739@qq.com
 * @date 2023-03-19 19:42:24
 */
public interface HomeAdvService extends IService<HomeAdvEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

