package com.qzq.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qzq.common.utils.PageUtils;
import com.qzq.gulimall.coupon.entity.SeckillSkuNoticeEntity;

import java.util.Map;

/**
 * 秒杀商品通知订阅
 *
 * @author quzongqian
 * @email 2959720739@qq.com
 * @date 2023-03-19 19:42:24
 */
public interface SeckillSkuNoticeService extends IService<SeckillSkuNoticeEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

