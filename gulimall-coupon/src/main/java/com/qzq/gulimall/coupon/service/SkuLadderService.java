package com.qzq.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qzq.common.utils.PageUtils;
import com.qzq.gulimall.coupon.entity.SkuLadderEntity;

import java.util.Map;

/**
 * 商品阶梯价格
 *
 * @author quzongqian
 * @email 2959720739@qq.com
 * @date 2023-03-19 19:42:24
 */
public interface SkuLadderService extends IService<SkuLadderEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveLadder(SkuLadderEntity skuLadderEntity);
}

