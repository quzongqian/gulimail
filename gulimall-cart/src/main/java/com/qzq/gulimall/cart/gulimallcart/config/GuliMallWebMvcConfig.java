package com.qzq.gulimall.cart.gulimallcart.config;

import com.qzq.gulimall.cart.gulimallcart.interceptor.CartInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class GuliMallWebMvcConfig implements WebMvcConfigurer {

    /**
     * 在进入controller之前，拦截所有路径，进入interceptor处理
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new CartInterceptor()).addPathPatterns("/**");
    }
}
