package com.qzq.gulimall.cart.gulimallcart.service.impl;
import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.google.common.collect.Lists;

import com.mysql.cj.xdevapi.JsonString;
import com.qzq.common.utils.R;
import com.qzq.gulimall.cart.gulimallcart.feign.ProductFeignService;
import com.qzq.gulimall.cart.gulimallcart.interceptor.CartInterceptor;
import com.qzq.gulimall.cart.gulimallcart.service.CartService;
import com.qzq.gulimall.cart.gulimallcart.to.UserInfoTo;
import com.qzq.gulimall.cart.gulimallcart.vo.CartItemVo;
import com.qzq.gulimall.cart.gulimallcart.vo.CartVo;
import com.qzq.gulimall.cart.gulimallcart.vo.SkuInfoVo;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class CartServiceImpl implements CartService {

    public  final String CART_PREFIX = "gulimall:cart:";

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private ThreadPoolExecutor executor;

    @Resource
    private ProductFeignService productFeignService;


    @Override
    public CartItemVo addToCart(Integer num, Long skuId) throws ExecutionException, InterruptedException {

        //找到要修改的购物车
        BoundHashOperations<String, Object, Object> cartOps = getCartOps();

        //判断原购物车是否有此项商品 如果无此项商品就直接添加购物车 如果有此项商品就在原有的基础上添加数量
        String res = (String) cartOps.get(skuId.toString());
        if (!StringUtils.isNotBlank(res)){
            //购物车无此商品
            //添加购物车信息
            //使用多线程异步编排 加快查询速度
            CartItemVo cartItemVo = new CartItemVo();
            CompletableFuture<Void> getSkuInfoFuture = CompletableFuture.runAsync(() -> {
                //远程调用查询
                R info = productFeignService.info(skuId);
                SkuInfoVo skuInfo = info.getData("skuInfo", new TypeReference<SkuInfoVo>(){});
                cartItemVo.setSkuId(skuId);
                cartItemVo.setCheck(true);
                cartItemVo.setTitle(skuInfo.getSkuTitle());
                cartItemVo.setImage(skuInfo.getSkuDefaultImg());
                cartItemVo.setPrice(skuInfo.getPrice());
                cartItemVo.setCount(num);
            }, executor);

            CompletableFuture<Void> getSkuAttrValuesFuture = CompletableFuture.runAsync(() -> {
                List<String> skuAttrValuesBySkuId = productFeignService.getSkuAttrValuesBySkuId(skuId);
                cartItemVo.setSkuAttrValues(skuAttrValuesBySkuId);
            }, executor);

            CompletableFuture.allOf(getSkuInfoFuture, getSkuAttrValuesFuture).get();

            //将准备好的购物车数据存入redis
            String s = JSON.toJSONString(cartItemVo);
            cartOps.put(skuId.toString(), s);

            return cartItemVo;
        } else {
            //购物车有此商品 修改数量
            CartItemVo cartItemVo = JSON.parseObject(res, CartItemVo.class);
            cartItemVo.setCount(cartItemVo.getCount() + num);

            cartOps.put(skuId.toString(), JSON.toJSONString(cartItemVo));
            return cartItemVo;

        }


    }

    @Override
    public CartItemVo addToCartSuccess(Long skuId) {

        BoundHashOperations<String, Object, Object> cartOps = getCartOps();
        String res = (String) cartOps.get(skuId.toString());
        return JSON.parseObject(res, CartItemVo.class);

    }

    @Override
    public CartVo getCart() throws ExecutionException, InterruptedException {

        // 判断购物车有没有临时用户
        // 如果有临时用户就把临时用户的购物车内容加入到用户购物车
        // 如果没有则直接展示用户购物车
        CartVo cart = new CartVo();
        UserInfoTo userInfoTo = CartInterceptor.threadLocal.get();
        if (userInfoTo.getUserId() != null){
            //用户登录 如果有临时用户就将购物车内容加入
            if (userInfoTo.getUserKey() != null){
                String userKey = CART_PREFIX + userInfoTo.getUserKey();
                List<CartItemVo> userKeyToCartItemVos = getCartItems(userKey);
                if (userKeyToCartItemVos != null && userKeyToCartItemVos.size() > 0){
                    for (CartItemVo cartItem : userKeyToCartItemVos) {
                        addToCart(cartItem.getCount(), cartItem.getSkuId());
                    }
                    //临时购物车商品添加完成以后清空临时购物车
                    clearCart(userKey);
                }

            }

            //获取登录过后的购物车数据【包含临时购物车】
            String cartKey = CART_PREFIX + userInfoTo.getUserId();
            List<CartItemVo> cartItemVos = getCartItems(cartKey);
            cart.setItems(cartItemVos);
            return cart;

        } else {
            //临时用户购物
            if (userInfoTo.getUserKey() != null){
                String userKey = CART_PREFIX + userInfoTo.getUserKey();
                List<CartItemVo> userKeyToCartItemVos = getCartItems(userKey);
                cart.setItems(userKeyToCartItemVos);
                return cart;
            }

            return null;

        }

    }




    private List<CartItemVo> getCartItems(String cartKey){
        BoundHashOperations<String, Object, Object> hashOps = stringRedisTemplate.boundHashOps(cartKey);
        List<Object> values = hashOps.values();
        if(values!=null && values.size()>0){
            List<CartItemVo> collect = values.stream().map((obj) -> {
                String str = (String) obj;
                CartItemVo cartItem = JSON.parseObject(str, CartItemVo.class);
                return cartItem;
            }).collect(Collectors.toList());
            return collect;
        }
        return null;
    }



    private BoundHashOperations<String, Object, Object> getCartOps() {
        UserInfoTo userInfoTo = CartInterceptor.threadLocal.get();
        //判断是登录以后的用户还是临时用户
        String user ;
        if (userInfoTo.getUserId() != null){
            user = CART_PREFIX + userInfoTo.getUserId();
        } else {
            user = CART_PREFIX + userInfoTo.getUserKey();
        }
        BoundHashOperations<String, Object, Object> stringObjectObjectBoundHashOperations = stringRedisTemplate.boundHashOps(user);
        return stringObjectObjectBoundHashOperations;
    }

    @Override
    public void clearCart(String cartKey){
        stringRedisTemplate.delete(cartKey);
    }

    @Override
    public void chickItem(Long skuId, Integer checked) {
        //获取购物项
        BoundHashOperations<String, Object, Object> cartOps = getCartOps();
        Object o = cartOps.get(skuId.toString());
        String s = (String) o;
        CartItemVo cartItemVo = JSON.parseObject(s, CartItemVo.class);
        //修改购物项
        cartItemVo.setCheck(checked == 1 ? true : false);
        String toJSONString = JSON.toJSONString(cartItemVo);
        cartOps.put(skuId.toString(), toJSONString);
    }

    @Override
    public void countItem(Long skuId, Integer num) {
        //获取购物项
        BoundHashOperations<String, Object, Object> cartOps = getCartOps();
        Object o = cartOps.get(skuId.toString());
        String s = (String) o;
        CartItemVo cartItemVo = JSON.parseObject(s, CartItemVo.class);
        //修改购物项
        cartItemVo.setCount(num);
        String toJSONString = JSON.toJSONString(cartItemVo);
        cartOps.put(skuId.toString(), toJSONString);
    }

    @Override
    public void deleteItem(Long skuId) {
        BoundHashOperations<String, Object, Object> cartOps = getCartOps();
        cartOps.delete(skuId.toString());
    }

    @Override
    public List<CartItemVo> getUserCartItems() {
        UserInfoTo userInfoTo = CartInterceptor.threadLocal.get();
        if (userInfoTo.getUserId() != null){
            String user = CART_PREFIX + userInfoTo.getUserId();
            List<CartItemVo> cartItems = getCartItems(user);
            List<CartItemVo> collect = cartItems.stream().filter(item -> {
                return item.getCheck();
            }).map(item -> {
                R price = productFeignService.getPrice(item.getSkuId());
                String data = (String) price.get("data");
                item.setPrice(new BigDecimal(data));
                return item;
            }).collect(Collectors.toList());
            return collect;
        } else {
            return null;
        }
    }
}
