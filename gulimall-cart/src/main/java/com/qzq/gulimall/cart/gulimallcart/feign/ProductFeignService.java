package com.qzq.gulimall.cart.gulimallcart.feign;

import com.qzq.common.utils.R;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.math.BigDecimal;
import java.util.List;

@FeignClient("gulimall-product")
public interface ProductFeignService {

    @RequestMapping("/product/skuinfo/info/{skuId}")
    R info(@PathVariable("skuId") Long skuId);


    @RequestMapping("/product/skusaleattrvalue/StringList/{skuId}")
    List<String> getSkuAttrValuesBySkuId(@PathVariable("skuId") Long skuId);


    @RequestMapping("/product/skuinfo/{skuId}/price")
    R getPrice(@PathVariable("skuId") Long skuId);
}
