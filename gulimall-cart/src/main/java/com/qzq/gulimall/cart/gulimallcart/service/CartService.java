package com.qzq.gulimall.cart.gulimallcart.service;

import com.qzq.gulimall.cart.gulimallcart.vo.CartItemVo;
import com.qzq.gulimall.cart.gulimallcart.vo.CartVo;

import java.util.List;
import java.util.concurrent.ExecutionException;

public interface CartService {
    /**
     * 添加购物车操作
     * @param num
     * @param skuId
     * @return
     * @throws ExecutionException
     * @throws InterruptedException
     */
    CartItemVo addToCart(Integer num, Long skuId) throws ExecutionException, InterruptedException;

    /**
     * 查找购物项操作
     * @param skuId
     * @return
     */
    CartItemVo addToCartSuccess(Long skuId);

    CartVo getCart() throws ExecutionException, InterruptedException;

    void clearCart(String cartKey);

    void chickItem(Long skuId, Integer checked);

    void countItem(Long skuId, Integer num);

    void deleteItem(Long skuId);

    List<CartItemVo> getUserCartItems();
}
