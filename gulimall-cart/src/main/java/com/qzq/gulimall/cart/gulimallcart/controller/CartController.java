package com.qzq.gulimall.cart.gulimallcart.controller;

import com.qzq.gulimall.cart.gulimallcart.interceptor.CartInterceptor;
import com.qzq.gulimall.cart.gulimallcart.service.CartService;
import com.qzq.gulimall.cart.gulimallcart.to.UserInfoTo;
import com.qzq.gulimall.cart.gulimallcart.vo.CartItemVo;
import com.qzq.gulimall.cart.gulimallcart.vo.CartVo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Controller
public class CartController {

    @Resource
    private CartService cartService;
    /**
     * 浏览器有一个cookie; user-key;标识用户身份，一个月后过期;
     * 如果第一次使用id的购物车功能，都会给一个临时的用户身份;
     * 浏览器以后保存，每次访问都会带上这个cookie;
     *
     * 登录:session有
     * 没登录:按照cookie里面带来user-key来做。
     * 第一次:如果没有临时用户，帮忙创建一个临时用户。
     */

    @GetMapping("/cart.html")
    public String cartList(Model model) throws ExecutionException, InterruptedException {


        CartVo cart = cartService.getCart();

        model.addAttribute("cart", cart);

        return "cartList";
    }

    @GetMapping("/userCartItems")
    @ResponseBody
    public List<CartItemVo> getUserCartItems(){
        return cartService.getUserCartItems();
    }


    @GetMapping("/addToCart")
    public String addToCart(@RequestParam("num") Integer num,
                            @RequestParam("skuId") Long skuId,
                            RedirectAttributes ra) throws ExecutionException, InterruptedException {

        cartService.addToCart(num, skuId);
        ra.addAttribute("skuId", skuId);
        return "redirect:http://cart.gulimall.com/addToCartSuccess.html";
    }

    @GetMapping("/addToCartSuccess.html")
    public String addToCartSuccess(@RequestParam("skuId") Long skuId,
                                   Model model){
        //重定向到页面，再次拿到CartItemVo即可
        CartItemVo cartItemVo = cartService.addToCartSuccess(skuId);
        model.addAttribute("item", cartItemVo);
        return "success";
    }

    @GetMapping("/checkItem")
    public String checkItem(@RequestParam("skuId") Long skuId,
                            @RequestParam("checked") Integer checked){

        cartService.chickItem(skuId, checked);

        return "redirect:http://cart.gulimall.com/cart.html";
    }

    @GetMapping("/countItem")
    public String countItem(@RequestParam("skuId") Long skuId,
                            @RequestParam("num") Integer num){

        cartService.countItem(skuId, num);

        return "redirect:http://cart.gulimall.com/cart.html";
    }

    @GetMapping("/deleteItem")
    public String deleteItem(@RequestParam("skuId") Long skuId){

        cartService.deleteItem(skuId);

        return "redirect:http://cart.gulimall.com/cart.html";
    }

}
