package com.qzq.gulimall.seckill.scheduled;

import com.qzq.gulimall.seckill.service.SeckillService;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class SeckillSkuScheduled {

    @Resource
    private SeckillService seckillServer;

    @Resource
    private RedissonClient redissonClient;

    public final String Upload_lock = "seckill:upload:lock";

    /**
     * TODO 幂等性问题 上架过的商品就不需要继续上架
     */
    @Scheduled(cron = "0 0 3 * * ?")
    public void uploadSeckillSkuLatest3Days(){

        log.info("上架秒杀商品中。。。");
        //设置一个分布式锁
        //锁的业务完成 状态已经更新完成  释放锁以后 其他人就会获得最新状态
        RLock lock = redissonClient.getLock(Upload_lock);
        lock.lock(10, TimeUnit.SECONDS);

        try {
            seckillServer.getLates3DaySession();
        } finally {
            lock.unlock();
        }

    }

}
