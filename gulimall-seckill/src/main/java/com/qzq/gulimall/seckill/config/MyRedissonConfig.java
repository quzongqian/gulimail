package com.qzq.gulimall.seckill.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@Configuration
public class MyRedissonConfig {

    @Bean(destroyMethod = "shutdown")
    public RedissonClient redissonClient() throws IOException {

        Config config = new Config();
        //单节点模式
        config.useSingleServer().setAddress("redis://192.168.231.222:6379");
        return Redisson.create(config);
    }
}
