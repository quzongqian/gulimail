package com.qzq.gulimall.ware.service.impl;

import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.qzq.common.to.SkuHasStockTo;
import com.qzq.common.to.mq.OrderTo;
import com.qzq.common.to.mq.StockDetailTo;
import com.qzq.common.to.mq.StockLockedTo;
import com.qzq.common.utils.R;
import com.qzq.common.exception.NoStockException;
import com.qzq.gulimall.ware.entity.WareOrderTaskDetailEntity;
import com.qzq.gulimall.ware.entity.WareOrderTaskEntity;
import com.qzq.gulimall.ware.feign.OrderFeignService;
import com.qzq.gulimall.ware.feign.ProductFeignService;
import com.qzq.gulimall.ware.service.WareOrderTaskDetailService;
import com.qzq.gulimall.ware.service.WareOrderTaskService;
import com.qzq.gulimall.ware.vo.OrderItemVo;
import com.qzq.gulimall.ware.vo.OrderVo;
import com.qzq.gulimall.ware.vo.WareSkuLockVo;
import lombok.Data;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qzq.common.utils.PageUtils;
import com.qzq.common.utils.Query;

import com.qzq.gulimall.ware.dao.WareSkuDao;
import com.qzq.gulimall.ware.entity.WareSkuEntity;
import com.qzq.gulimall.ware.service.WareSkuService;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


@Service("wareSkuService")
public class WareSkuServiceImpl extends ServiceImpl<WareSkuDao, WareSkuEntity> implements WareSkuService {

    @Autowired
    private WareSkuDao wareSkuDao;

    @Autowired
    private ProductFeignService productFeignService;

    @Resource
    private WareOrderTaskService wareOrderTaskService;

    @Resource
    private WareOrderTaskDetailService wareOrderTaskDetailService;

    @Resource
    private RabbitTemplate rabbitTemplate;

    @Resource
    private OrderFeignService orderFeignService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<WareSkuEntity> page = this.page(
                new Query<WareSkuEntity>().getPage(params),
                new QueryWrapper<WareSkuEntity>()
        );

        return new PageUtils(page);
    }



    @Override
    public PageUtils queryPageWareSkuInfo(Map<String, Object> params) {
        //skuId:
        //wareId:
        LambdaQueryWrapper<WareSkuEntity> queryWrapper = new LambdaQueryWrapper<>();

        String skuId = (String) params.get("skuId");
        if (StringUtils.isNotBlank(skuId)){
            queryWrapper.eq(WareSkuEntity::getSkuId, skuId);
        }

        String wareId = (String) params.get("wareId");
        if (StringUtils.isNotBlank(wareId)){
            queryWrapper.eq(WareSkuEntity::getId, wareId);
        }

        IPage<WareSkuEntity> page = this.page(
                new Query<WareSkuEntity>().getPage(params),
                queryWrapper
        );

        return new PageUtils(page);
    }

    @Override
    public void saveWareSku(Long skuId, Long wareId, Integer skuNum) {
        LambdaQueryWrapper<WareSkuEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(WareSkuEntity::getSkuId, skuId).eq(WareSkuEntity::getWareId, wareId);
        WareSkuEntity wareSkuEntity = this.getOne(queryWrapper);

        //判断是否之前有此库存 如果有此库存就在原先的基础上添加 如果没有创建新的库存信息
        if (wareSkuEntity == null){
            //新建一个库存信息
            WareSkuEntity wareSku = new WareSkuEntity();
            wareSku.setSkuId(skuId);
            wareSku.setStockLocked(0);
            wareSku.setWareId(wareId);
            wareSku.setStock(skuNum);

            try {
                R info = productFeignService.info(skuId);
                Map<String, Object> skuInfo = (Map<String, Object>) info.get("skuInfo");
                if (info.getCode() == 0){
                    String skuName = (String) skuInfo.get("skuName");
                    wareSku.setSkuName(skuName);
                }
            } catch (Exception e){

            }

            this.baseMapper.insert(wareSku);
        } else {
            //修改库存信息
            this.baseMapper.updateStock(skuId, wareId, skuNum);
        }

    }

    @Override
    public List<SkuHasStockTo> hasStock(List<Long> skuId) {

        List<SkuHasStockTo> collect = skuId.stream().map(id -> {
            SkuHasStockTo skuHasStockTo = new SkuHasStockTo();
            skuHasStockTo.setSkuId(id);
            Long count = baseMapper.getStockCount(id);
            skuHasStockTo.setHasStock(count == null ? false : count > 0);
            return skuHasStockTo;
        }).collect(Collectors.toList());

        return collect;
    }


    /**
     * 锁定库存
     * @param vo
     * @return
     */
    @Override
    @Transactional
    public Boolean orderLockStock(WareSkuLockVo vo) {
//        /**
//         * 保存库存工作单的详情。
//         * 追溯。
//         */
        WareOrderTaskEntity taskEntity = new WareOrderTaskEntity();
        taskEntity.setOrderSn(vo.getOrderSn());
        wareOrderTaskService.save(taskEntity);


        //1、按照下单的收货地址，找到一个就近仓库，锁定库存。
        //1、找到每个商品在哪个仓库都有库存
        List<OrderItemVo> locks = vo.getLocks();

        List<SkuWareHasStock> collect = locks.stream().map(item -> {
            SkuWareHasStock stock = new SkuWareHasStock();
            Long skuId = item.getSkuId();
            stock.setSkuId(skuId);
            stock.setNum(item.getCount());
            //查询这个商品在哪里有库存
            List<Long> wareIds = wareSkuDao.listWareIdHasSkuStock(skuId);
            stock.setWareId(wareIds);
            return stock;
        }).collect(Collectors.toList());

        //2、锁定库存
        for (SkuWareHasStock hasStock : collect) {
            Boolean skuStocked = false;
            Long skuId = hasStock.getSkuId();
            List<Long> wareIds = hasStock.getWareId();
            if (wareIds == null || wareIds.size() == 0) {
                //没有任何仓库有这个商品的库存
                throw new NoStockException(skuId);
            }
            //1、如果每一个商品都锁定成功，将当前商品锁定了几件的工作单记录发给MQ
            //2、锁定失败。前面保存的工作单信息就回滚了。发送出去的消息，即使要解锁记录，由于去数据库查不到id，所以就不用解锁
            //     1： 1 - 2 - 1   2：2-1-2  3：3-1-1(x)
            for (Long wareId : wareIds) {
                //成功就返回1,否则就是0
                Long count = wareSkuDao.lockSkuStock(skuId, wareId, hasStock.getNum());
                if (count == 1) {
                    skuStocked = true;
                    //TODO 告诉MQ库存锁定成功
                    WareOrderTaskDetailEntity entity = new WareOrderTaskDetailEntity(null, skuId, "", hasStock.getNum(), taskEntity.getId(), wareId, 1);
                    wareOrderTaskDetailService.save(entity);
                    StockLockedTo lockedTo = new StockLockedTo();
                    lockedTo.setId(taskEntity.getId());
                    StockDetailTo stockDetailTo = new StockDetailTo();
                    BeanUtils.copyProperties(entity, stockDetailTo);
                    //只发id不行，防止回滚以后找不到数据
                    lockedTo.setDetail(stockDetailTo);
                    rabbitTemplate.convertAndSend("stock-event-exchange", "stock.locked", lockedTo);
                    break;
                } else {
                    //当前仓库锁失败，重试下一个仓库
                }
            }
            if (skuStocked == false) {
                //当前商品所有仓库都没有锁住
                throw new NoStockException(skuId);
            }
        }


        //3、肯定全部都是锁定成功过的

        return true;
    }

    /**
     * 补偿解锁
     * @param stockLockedTo
     */
    @Override
    public void unLockStock(StockLockedTo stockLockedTo) {
        StockDetailTo detail = stockLockedTo.getDetail();
        Long detailId = detail.getId();
        //查询 detailId 是否有记录
        //如果没有记录  就说明是锁定库存时发生错误 库存已经回滚 无需解锁
        //如果有记录  证明数据锁定成功
        //      解锁 ： 订单状态
        //          1.没有这个订单。必须解锁
        //          2.有这个订单 查看解锁状态
        //              订单状态
        //                  取消 一定需要解锁
        //                  没取消 不需要解锁
        WareOrderTaskDetailEntity byId = wareOrderTaskDetailService.getById(detailId);
        if (byId != null){
            //需要解锁
            Long id = stockLockedTo.getId();
            WareOrderTaskEntity wareOrderTask = wareOrderTaskService.getById(id);
            String orderSn = wareOrderTask.getOrderSn();
            //远程查询订单 查看订单状态是否是已取消 如果是需要解锁
            R order = orderFeignService.getByStatusByOrderSn(orderSn);
            if (order.getCode() == 0){
                OrderVo data = order.getData(new TypeReference<OrderVo>() {
                });
                if (data == null || data.getStatus() == 4){
                    //订单不存在 | 订单已取消状态 解锁库存
                    if (byId.getLockStatus() == 1) {
                        //只有为已锁定状态才可已解锁
                        wareSkuDao.releaseLockStock(detail.getSkuId(), detail.getWareId(), detail.getSkuNum());
                        wareSkuDao.releaseLockStockup(detail.getSkuId(), detail.getWareId(), detail.getSkuNum());
                        WareOrderTaskDetailEntity wareOrderTaskDetailEntity = new WareOrderTaskDetailEntity();
                        wareOrderTaskDetailEntity.setId(detailId);
                        wareOrderTaskDetailEntity.setLockStatus(2);
                        wareOrderTaskDetailService.updateById(wareOrderTaskDetailEntity);
                    }

                }

            } else {
                //拒绝消息 重新放回队列 让别人重新消费
                throw new RuntimeException("远程服务失败");
            }
        }
    }

    /**
     * 主动解锁
     * @param order
     */
    @Override
    @Transactional
    public void unLockStock(OrderTo order) {
        WareOrderTaskEntity wareOrderTask = wareOrderTaskService.getOne(new LambdaQueryWrapper<WareOrderTaskEntity>()
                .eq(WareOrderTaskEntity::getOrderSn, order.getOrderSn()));

        List<WareOrderTaskDetailEntity> detailEntities = wareOrderTaskDetailService.list(new LambdaQueryWrapper<WareOrderTaskDetailEntity>()
                .eq(WareOrderTaskDetailEntity::getTaskId, wareOrderTask.getId())
                .eq(WareOrderTaskDetailEntity::getLockStatus, 1));

        for (WareOrderTaskDetailEntity detail : detailEntities) {
            wareSkuDao.releaseLockStock(detail.getSkuId(), detail.getWareId(), detail.getSkuNum());
            wareSkuDao.releaseLockStockup(detail.getSkuId(), detail.getWareId(), detail.getSkuNum());
            WareOrderTaskDetailEntity wareOrderTaskDetailEntity = new WareOrderTaskDetailEntity();
            wareOrderTaskDetailEntity.setId(detail.getId());
            wareOrderTaskDetailEntity.setLockStatus(2);
            wareOrderTaskDetailService.updateById(wareOrderTaskDetailEntity);
        }

    }

    @Data
    class SkuWareHasStock {
        private Long skuId;
        private Integer num;
        private List<Long> wareId;
    }
}