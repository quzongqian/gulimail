package com.qzq.gulimall.ware.feign;

import com.qzq.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient("gulimall-order")
public interface OrderFeignService {

    @GetMapping("/order/order/status/{orderSn}")
    R getByStatusByOrderSn(@PathVariable("orderSn") String orderSn);

}
