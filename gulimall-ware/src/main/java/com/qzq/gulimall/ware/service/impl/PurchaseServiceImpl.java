package com.qzq.gulimall.ware.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.qzq.common.constant.WareConstant;
import com.qzq.common.utils.Constant;
import com.qzq.gulimall.ware.entity.PurchaseDetailEntity;
import com.qzq.gulimall.ware.service.PurchaseDetailService;
import com.qzq.gulimall.ware.service.WareSkuService;
import com.qzq.gulimall.ware.vo.DonePurchaseVo;
import com.qzq.gulimall.ware.vo.DoneVo;
import com.qzq.gulimall.ware.vo.MergeVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qzq.common.utils.PageUtils;
import com.qzq.common.utils.Query;

import com.qzq.gulimall.ware.dao.PurchaseDao;
import com.qzq.gulimall.ware.entity.PurchaseEntity;
import com.qzq.gulimall.ware.service.PurchaseService;
import org.springframework.transaction.annotation.Transactional;


@Service("purchaseService")
public class PurchaseServiceImpl extends ServiceImpl<PurchaseDao, PurchaseEntity> implements PurchaseService {

    @Autowired
    private PurchaseDetailService purchaseDetailService;

    @Autowired
    public WareSkuService wareSkuService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<PurchaseEntity> page = this.page(
                new Query<PurchaseEntity>().getPage(params),
                new QueryWrapper<PurchaseEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPageunReceive(Map<String, Object> params) {
        LambdaQueryWrapper<PurchaseEntity> queryWrapper = new LambdaQueryWrapper<>();

        queryWrapper.eq(PurchaseEntity::getStatus, 0).or().eq(PurchaseEntity::getStatus, 1);

        IPage<PurchaseEntity> page = this.page(
                new Query<PurchaseEntity>().getPage(params),
                queryWrapper
        );

        return new PageUtils(page);
    }

    @Override
    @Transactional
    public void saveMerge(MergeVo mergeVo) {
        Long purchaseId = mergeVo.getPurchaseId();

        //判断这个采购单是否存在 不存在则新建一个采购单
        if (purchaseId == null){
            PurchaseEntity purchaseEntity = new PurchaseEntity();
            purchaseEntity.setCreateTime(new Date());
            purchaseEntity.setUpdateTime(new Date());
            purchaseEntity.setStatus(WareConstant.PurchaseStatusMenu.CREATED.getCode());
            this.save(purchaseEntity);
            purchaseId = purchaseEntity.getId();
        }

        //TODO 如果根据采购单状态判断是否可以分配
        //修改采购需求
        Long finalPurchaseId = purchaseId;
        List<Long> purchaseDetails = mergeVo.getItems();
        List<PurchaseDetailEntity> collect = purchaseDetails.stream().map(item -> {
            PurchaseDetailEntity purchaseDetailEntity = new PurchaseDetailEntity();
            purchaseDetailEntity.setId(item);
            purchaseDetailEntity.setPurchaseId(finalPurchaseId);
            purchaseDetailEntity.setStatus(WareConstant.PurchaseDetailStatusMenu.ASSIGEND.getCode());
            return purchaseDetailEntity;
        }).collect(Collectors.toList());
        purchaseDetailService.updateBatchById(collect);

        PurchaseEntity purchaseEntity = new PurchaseEntity();
        purchaseEntity.setUpdateTime(new Date());
        purchaseEntity.setId(finalPurchaseId);
        this.updateById(purchaseEntity);
    }

    @Override
    @Transactional
    public void received(List<Long> ids) {
        // 1.确认当前采购单是已分配状态或者创建状态
        List<PurchaseEntity> purchaseEntities = ids.stream().map((item) -> {
            PurchaseEntity purchaseEntity = this.getById(item);
            return purchaseEntity;
        }).filter(entity -> {
            if (entity.getStatus() == WareConstant.PurchaseStatusMenu.ASSIGEND.getCode() ||
                    entity.getStatus() == WareConstant.PurchaseStatusMenu.CREATED.getCode()) {
                return true;
            }
            return false;
        }).map(item -> {
            item.setStatus(WareConstant.PurchaseStatusMenu.RECEIVE.getCode());
            item.setUpdateTime(new Date());
            return item;
        }).collect(Collectors.toList());

        // 2.修改采购单
        this.updateBatchById(purchaseEntities);

        // 3.修改采购项
        purchaseEntities.forEach(item -> {
            List<PurchaseDetailEntity> purchaseDetailEntityList = purchaseDetailService.listDetailsByPurchaseId(item.getId());
            List<PurchaseDetailEntity> collect = purchaseDetailEntityList.stream().map(i -> {
                PurchaseDetailEntity purchaseDetailEntity = new PurchaseDetailEntity();
                purchaseDetailEntity.setId(i.getId());
                purchaseDetailEntity.setStatus(WareConstant.PurchaseDetailStatusMenu.BUYING.getCode());
                return purchaseDetailEntity;
            }).collect(Collectors.toList());
            purchaseDetailService.updateBatchById(collect);
        });
    }

    @Override
    @Transactional
    public void done(DoneVo doneVo) {
        //1.修改采购单 采购项全部成功为成功 采购项只要有一个失败为异常
        Long purchaseId = doneVo.getId();
        //2.修改采购项
        Boolean flag = true;
        List<DonePurchaseVo> detailItems = doneVo.getItems();
        for (DonePurchaseVo detailItem : detailItems) {
            PurchaseDetailEntity purchaseDetailEntity = new PurchaseDetailEntity();
            //查看采购项的状态 如果成功修改状态为成功 失败修改为采购失败
            if (detailItem.getStatus() == WareConstant.PurchaseDetailStatusMenu.FINISH.getCode()){
                //成功
                purchaseDetailEntity.setId(detailItem.getItemId());
                purchaseDetailEntity.setStatus(detailItem.getStatus());
                //3.成功的采购项入库
                PurchaseDetailEntity detailEntity = purchaseDetailService.getById(detailItem.getItemId());
                wareSkuService.saveWareSku(detailEntity.getSkuId(), detailEntity.getWareId(), detailEntity.getSkuNum());

            } else {
                //失败
                flag = false;
                purchaseDetailEntity.setId(detailItem.getItemId());
                purchaseDetailEntity.setStatus(WareConstant.PurchaseDetailStatusMenu.HASERROR.getCode());
            }
            purchaseDetailService.updateById(purchaseDetailEntity);
        }

        PurchaseEntity purchaseEntity = new PurchaseEntity();
        purchaseEntity.setId(purchaseId);
        purchaseEntity.setUpdateTime(new Date());
        if (flag == true){
            purchaseEntity.setStatus(WareConstant.PurchaseStatusMenu.FINISH.getCode());
        } else {
            purchaseEntity.setStatus(WareConstant.PurchaseStatusMenu.HASERROR.getCode());
        }
        this.updateById(purchaseEntity);

    }

}