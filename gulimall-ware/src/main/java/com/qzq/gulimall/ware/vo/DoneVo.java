package com.qzq.gulimall.ware.vo;

import lombok.Data;

import java.util.List;

@Data
public class DoneVo {
    private Long id;
    private List<DonePurchaseVo> items;
}
