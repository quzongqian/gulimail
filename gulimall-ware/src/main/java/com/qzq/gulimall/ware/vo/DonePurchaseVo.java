package com.qzq.gulimall.ware.vo;

import lombok.Data;

@Data
public class DonePurchaseVo {
    private Long itemId;
    private Integer status;
    private String reason;

}
