package com.qzq.gulimall.ware.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.qzq.common.constant.WareConstant;
import com.qzq.gulimall.ware.entity.PurchaseDetailEntity;
import com.qzq.gulimall.ware.service.PurchaseDetailService;
import com.qzq.gulimall.ware.vo.DoneVo;
import com.qzq.gulimall.ware.vo.MergeVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.qzq.gulimall.ware.entity.PurchaseEntity;
import com.qzq.gulimall.ware.service.PurchaseService;
import com.qzq.common.utils.PageUtils;
import com.qzq.common.utils.R;



/**
 * 采购信息
 *
 * @author quzongqian
 * @email 2959720739@qq.com
 * @date 2023-03-19 20:54:24
 */
@RestController
@RequestMapping("ware/purchase")
public class PurchaseController {
    @Autowired
    private PurchaseService purchaseService;

    @Autowired
    private PurchaseDetailService purchaseDetailService;


    ///ware/purchase/done
    @PostMapping("/done")
    public R done(@RequestBody DoneVo doneVo){

        purchaseService.done(doneVo);

        return R.ok();
    }

    @PostMapping("/merge")
    public R saveMerge(@RequestBody MergeVo mergeVo){

        purchaseService.saveMerge(mergeVo);

        return R.ok();
    }


    @PostMapping("received")
    public R receiver(@RequestBody List<Long> ids) {
        purchaseService.received(ids);
        return R.ok();
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("ware:purchase:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = purchaseService.queryPage(params);

        return R.ok().put("page", page);
    }

    @RequestMapping("/unreceive/list")
    //@RequiresPermissions("ware:purchase:list")
    public R listfindUn(@RequestParam Map<String, Object> params){
        PageUtils page = purchaseService.queryPageunReceive(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("ware:purchase:info")
    public R info(@PathVariable("id") Long id){
		PurchaseEntity purchase = purchaseService.getById(id);

        return R.ok().put("purchase", purchase);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("ware:purchase:save")
    public R save(@RequestBody PurchaseEntity purchase){
        purchase.setCreateTime(new Date());
        purchase.setUpdateTime(new Date());
		purchaseService.save(purchase);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("ware:purchase:update")
    public R update(@RequestBody PurchaseEntity purchase){
		purchaseService.updateById(purchase);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("ware:purchase:delete")
    public R delete(@RequestBody Long[] ids){
		purchaseService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
