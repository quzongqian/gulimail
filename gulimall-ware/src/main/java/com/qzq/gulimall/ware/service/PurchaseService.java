package com.qzq.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qzq.common.utils.PageUtils;
import com.qzq.gulimall.ware.entity.PurchaseEntity;
import com.qzq.gulimall.ware.vo.DoneVo;
import com.qzq.gulimall.ware.vo.MergeVo;

import java.util.List;
import java.util.Map;

/**
 * 采购信息
 *
 * @author quzongqian
 * @email 2959720739@qq.com
 * @date 2023-03-19 20:54:24
 */
public interface PurchaseService extends IService<PurchaseEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPageunReceive(Map<String, Object> params);

    void saveMerge(MergeVo mergeVo);

    void received(List<Long> ids);

    void done(DoneVo doneVo);
}

