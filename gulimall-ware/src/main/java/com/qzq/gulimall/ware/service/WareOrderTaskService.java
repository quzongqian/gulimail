package com.qzq.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qzq.common.utils.PageUtils;
import com.qzq.gulimall.ware.entity.WareOrderTaskEntity;

import java.util.Map;

/**
 * 库存工作单
 *
 * @author quzongqian
 * @email 2959720739@qq.com
 * @date 2023-03-19 20:54:24
 */
public interface WareOrderTaskService extends IService<WareOrderTaskEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

