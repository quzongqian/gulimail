package com.qzq.gulimall.ware.dao;

import com.qzq.gulimall.ware.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author quzongqian
 * @email 2959720739@qq.com
 * @date 2023-03-19 20:54:24
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
