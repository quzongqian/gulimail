package com.qzq.gulimall.ware.listener;

import com.qzq.common.to.mq.OrderTo;
import com.qzq.common.to.mq.StockLockedTo;
import com.qzq.gulimall.ware.service.WareSkuService;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;

@RabbitListener(queues = "stock.release.stock.queue")
@Service
public class MqListener {

    @Resource
    private WareSkuService wareSkuService;

    @RabbitHandler
    public void HandlerReleaseStock(StockLockedTo stockLockedTo, Message message, Channel channel) throws IOException {
        try {
            wareSkuService.unLockStock(stockLockedTo);
            //手动确认
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        } catch (Exception e){
            //拒绝消息 放回队列
            channel.basicReject(message.getMessageProperties().getDeliveryTag(), true);
        }

    }


    @RabbitHandler
    public void HandlerReleaseStock(OrderTo order, Message message, Channel channel) throws IOException {
        try {
            wareSkuService.unLockStock(order);
            //手动确认
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        } catch (Exception e){
            //拒绝消息 放回队列
            channel.basicReject(message.getMessageProperties().getDeliveryTag(), true);
        }

    }

}
