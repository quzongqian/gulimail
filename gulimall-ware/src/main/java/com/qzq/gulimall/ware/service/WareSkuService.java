package com.qzq.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qzq.common.to.SkuHasStockTo;
import com.qzq.common.to.mq.OrderTo;
import com.qzq.common.to.mq.StockLockedTo;
import com.qzq.common.utils.PageUtils;
import com.qzq.gulimall.ware.entity.WareSkuEntity;
import com.qzq.gulimall.ware.vo.WareSkuLockVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 商品库存
 *
 * @author quzongqian
 * @email 2959720739@qq.com
 * @date 2023-03-19 20:54:24
 */
public interface WareSkuService extends IService<WareSkuEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPageWareSkuInfo(Map<String, Object> params);

    void saveWareSku(Long skuId, Long wareId, Integer skuNum);

    List<SkuHasStockTo> hasStock( List<Long> skuId);

    Boolean orderLockStock(WareSkuLockVo vo);

    void unLockStock(StockLockedTo stockLockedTo);

    void unLockStock(OrderTo order);
}

