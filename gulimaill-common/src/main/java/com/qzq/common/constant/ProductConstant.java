package com.qzq.common.constant;

import java.awt.*;

public class ProductConstant {

    public enum AttrMenu{
        ATTR_TYPE_BASE(1, "基本属性"),ATTR_TYPE_SLAE(0, "销售属性");
        private int code;
        private String msg;

        AttrMenu(int code, String msg){
            this.code = code;
            this.msg = msg;
        }

        public String getMsg() {
            return msg;
        }

        public int getCode() {
            return code;
        }
    };

    public enum StatusMenu{
        NEW_SPU(0, "新建"),SPU_UP(1, "商品上架"),SUP_DOWN(2,"商品下架");
        private int code;
        private String msg;

        StatusMenu(int code, String msg){
            this.code = code;
            this.msg = msg;
        }

        public String getMsg() {
            return msg;
        }

        public int getCode() {
            return code;
        }
    };
}
