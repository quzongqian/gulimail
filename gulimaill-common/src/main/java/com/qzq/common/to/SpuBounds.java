package com.qzq.common.to;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class SpuBounds {
    private Long SpuId;
    private BigDecimal buyBounds;
    private BigDecimal growBounds;
}
