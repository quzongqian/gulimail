package com.qzq.common.exception;

/**
 * 10:通用
 * 11：商品
 * 12订单
 * 13购物车
 * 14物流
 * 15用户
 *
 */
public enum BizCodeEnums {
    UNKNOW_EXCEPTION(10000, "系统未知异常"),
    VAILD_EXCEPTION(10001,"参数格式检验失败"),
    SMS_CODE_EXCEPTION(10002,"验证码获取频率过高，请稍后在试！"),
    TOO_MANY_REQUEST(10003, "请求流量过大"),
    PRODUCT_UP_EXCEPTION(11000, "商品上架异常"),
    USERNAME_EXIST_EXCEPTION(15001, "用户存在异常"),
    PHONE_EXIST_EXCEPTION(15002, "手机号存在异常"),
    LOGINACCT_PASSWORD_INVAILD_EXCEPTION(15003,"账号密码错误"),
    NO_STOCK_EXCEPTION(21000, "商品库存不足");

    private int code;
    private String msg;

    BizCodeEnums(int code, String msg){
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
