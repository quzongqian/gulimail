package com.qzq.gulimall.product.service.impl;

import com.qzq.gulimall.product.vo.skuItemVo.SkuItemSaleAttrVo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qzq.common.utils.PageUtils;
import com.qzq.common.utils.Query;

import com.qzq.gulimall.product.dao.SkuSaleAttrValueDao;
import com.qzq.gulimall.product.entity.SkuSaleAttrValueEntity;
import com.qzq.gulimall.product.service.SkuSaleAttrValueService;


@Service("skuSaleAttrValueService")
public class SkuSaleAttrValueServiceImpl extends ServiceImpl<SkuSaleAttrValueDao, SkuSaleAttrValueEntity> implements SkuSaleAttrValueService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SkuSaleAttrValueEntity> page = this.page(
                new Query<SkuSaleAttrValueEntity>().getPage(params),
                new QueryWrapper<SkuSaleAttrValueEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void saveSkuAsleAttrValue(List<SkuSaleAttrValueEntity> skuSaleAttrValueEntities) {
        this.saveBatch(skuSaleAttrValueEntities);
    }

    @Override
    public List<SkuItemSaleAttrVo> getSaleAttrsBySpuId(Long spuId) {
        List<SkuItemSaleAttrVo> sale = this.baseMapper.getSaleAttrsBySpuId(spuId);
        return sale;
    }

    @Override
    public List<String> getSkuAttrValuesBySkuId(Long skuId) {
        List<String> list =  this.baseMapper.getSkuAttrValuesBySkuId(skuId);
        return list;
    }

}