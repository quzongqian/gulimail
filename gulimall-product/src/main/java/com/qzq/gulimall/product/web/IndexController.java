package com.qzq.gulimall.product.web;

import com.qzq.gulimall.product.entity.CategoryEntity;
import com.qzq.gulimall.product.service.CategoryService;
import com.qzq.gulimall.product.vo.Catalogs2Vo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Controller
public class IndexController {

    @Resource
    private CategoryService categoryService;


    @GetMapping({"/", "/index"})
    public String indexPage(Model model){

        List<CategoryEntity> categories =  categoryService.getCatLevel1();

        model.addAttribute("categories", categories);

        return "index";
    }

    /**
     * 二级、三级分类数据
     *
     * @return
     */
    @GetMapping(value = "index/json/catalog.json")
    @ResponseBody
    public Map<String, List<Catalogs2Vo>> getCatalogJson() {
        return categoryService.getCatalogJson();
    }


}
