package com.qzq.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qzq.common.utils.PageUtils;
import com.qzq.gulimall.product.entity.SkuImagesEntity;

import java.util.List;
import java.util.Map;

/**
 * sku图片
 *
 * @author quzongqian
 * @email 2959720739@qq.com
 * @date 2023-03-19 16:29:46
 */
public interface SkuImagesService extends IService<SkuImagesEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveSkuImages(List<SkuImagesEntity> imagesEntityList);

    List<SkuImagesEntity> getImagesBySkuId(Long skuId);
}

