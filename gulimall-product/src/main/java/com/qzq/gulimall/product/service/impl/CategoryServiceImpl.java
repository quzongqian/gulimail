package com.qzq.gulimall.product.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qzq.common.utils.PageUtils;
import com.qzq.common.utils.Query;
import com.qzq.gulimall.product.dao.CategoryDao;
import com.qzq.gulimall.product.entity.CategoryEntity;
import com.qzq.gulimall.product.service.CategoryBrandRelationService;
import com.qzq.gulimall.product.service.CategoryService;
import com.qzq.gulimall.product.vo.Catalogs2Vo;
import org.apache.commons.lang.StringUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;


@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {
    @Resource
    private CategoryBrandRelationService categoryBrandRelationService;

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private RedissonClient redissonClient;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<CategoryEntity> listWithTree() {
        //1.查出所有分类
        List<CategoryEntity> categoryEntities = baseMapper.selectList(null);

        //2.组装成父子结构的树形结构 找child
        List<CategoryEntity> level1 = categoryEntities.stream().filter(categoryEntity -> {
            return categoryEntity.getParentCid() == 0;
        }).map((menu) -> {
            menu.setChildren(getChildrens(menu, categoryEntities));
            return menu;
        }).sorted((n1, n2) -> {
            return n1.getSort() - n2.getSort();
        }).collect(Collectors.toList());


        return level1;
    }


    //递归查询所有菜单的子菜单
    private List<CategoryEntity> getChildrens(CategoryEntity root, List<CategoryEntity> all) {

        List<CategoryEntity> childMenu = all.stream().filter(categoryEntity -> {
            return categoryEntity.getParentCid() == root.getCatId();
        }).map(categoryEntity -> {
            categoryEntity.setChildren(getChildrens(categoryEntity, all));
            return categoryEntity;
        }).sorted((n1, n2) -> {
            return (n1.getSort() == null ? 0 : n1.getSort()) - (n2.getSort() == null ? 0 : n2.getSort());
        }).collect(Collectors.toList());
        return childMenu;
    }

    @Override
    public void removeMenusByIds(List<Long> asList) {
        //TODO 检测当前删除的菜单，是否被其他地方引用
        //逻辑删除
        baseMapper.deleteBatchIds(asList);
    }

    @Override
    public Long[] findParentCidByCatelogId(Long catelogId) {

        List<Long> paths = new ArrayList<>();
        List<Long> list = findParent(catelogId, paths);
        Collections.reverse(list);
        return list.toArray(new Long[list.size()]);
    }

    @Override
    @Transactional
//    @CachePut //双写模式
    @Caching(evict = {    //失效模式
            @CacheEvict(value = {"category"}, key = "'level1Catalog'"),
            @CacheEvict(value = {"category"}, key = "'getCatalogJson'")
    })
    public void updateCascade(CategoryEntity category) {
        this.updateById(category);
        categoryBrandRelationService.casecadeUpdate(category.getCatId(), category.getName());
    }

    @Override
    @Cacheable(value = {"category"}, key = "'level1Catalog'")
    public List<CategoryEntity> getCatLevel1() {
        System.out.println("获取l1数据");
        LambdaQueryWrapper<CategoryEntity> categoryEntityLambdaQueryWrapper = new LambdaQueryWrapper<>();
        categoryEntityLambdaQueryWrapper.eq(CategoryEntity::getParentCid, 0);
        List<CategoryEntity> list = this.list(categoryEntityLambdaQueryWrapper);

        return list;
    }


    private List<Long> findParent(Long catelogId, List<Long> paths) {
        CategoryEntity byId = this.getById(catelogId);
        paths.add(byId.getCatId());
        if (byId.getParentCid() != 0) {
            findParent(byId.getParentCid(), paths);
        }
        return paths;
    }


    @Override
    @Cacheable(value = "category", key = "'getCatalogJson'")
    public Map<String, List<Catalogs2Vo>> getCatalogJson() {
        System.out.println("查询数据库....");
        // 性能优化：将数据库的多次查询变为一次
        List<CategoryEntity> selectList = this.baseMapper.selectList(null);

        //1、查出所有分类
        //1、1）查出所有一级分类
        List<CategoryEntity> level1Categories = getParentCid(selectList, 0L);

        //封装数据
        Map<String, List<Catalogs2Vo>> parentCid = level1Categories.stream().collect(Collectors.toMap(k -> k.getCatId().toString(), v -> {
            //1、每一个的一级分类,查到这个一级分类的二级分类
            List<CategoryEntity> categoryEntities = getParentCid(selectList, v.getCatId());

            //2、封装上面的结果
            List<Catalogs2Vo> catalogs2Vos = null;
            if (categoryEntities != null) {
                catalogs2Vos = categoryEntities.stream().map(l2 -> {
                    Catalogs2Vo catalogs2Vo = new Catalogs2Vo(v.getCatId().toString(), null, l2.getCatId().toString(), l2.getName().toString());

                    //1、找当前二级分类的三级分类封装成vo
                    List<CategoryEntity> level3Catelog = getParentCid(selectList, l2.getCatId());

                    if (level3Catelog != null) {
                        List<Catalogs2Vo.Category3Vo> category3Vos = level3Catelog.stream().map(l3 -> {
                            //2、封装成指定格式
                            Catalogs2Vo.Category3Vo category3Vo = new Catalogs2Vo.Category3Vo(l2.getCatId().toString(), l3.getCatId().toString(), l3.getName());

                            return category3Vo;
                        }).collect(Collectors.toList());
                        catalogs2Vo.setCatalog3List(category3Vos);
                    }

                    return catalogs2Vo;
                }).collect(Collectors.toList());
            }

            return catalogs2Vos;
        }));

        String s = JSON.toJSONString(parentCid);
        stringRedisTemplate.opsForValue().set("catalogJSON", s, 1, TimeUnit.DAYS);

        return parentCid;
    }

    /**
     * TODO 会产生堆外内存溢出 OutOfDirectMemoryError
     * 1)、springboot2.0以后默认使用Lettuce作为操作redis的客户端。它使用netty进行网络通信。
     * 2)、lettuce的bug导致netty堆外内存溢出 -Xmx300m; netty如果没有指定堆外内存，默认使用-Xmx300m
     *     可以通过-Dio.netty.maxDirectMemory进行设置
     * 解决方案:不能使用-Dio.netty.maxDirectMemorv只去调大堆外内存
     * 1) 、升级Lettuce客户端。2)、切换便用jedis
     *
     * @return
     */
    public Map<String, List<Catalogs2Vo>> getCatalogJson2() {


        //1、空结果缓存，解决缓存穿透
        //2、设置过期时间-(加随机值):解决缓存雪崩
        //3、加锁:解决缓存击穿

        //先从缓存中获取json数据 看看redis中有没有
        String catalogJSON = stringRedisTemplate.opsForValue().get("catalogJSON");
        if (StringUtils.isEmpty(catalogJSON)) {
            System.out.println("缓存未命中,查询数据库");
            //如果没有就从数据库中获取数据 转换成json对象 保存到redis中
            Map<String, List<Catalogs2Vo>> catalogJsonFromDB = getCatalogJsonFromDBWithRedisLock();

            return catalogJsonFromDB;
        }
        System.out.println("缓存命中，直接返回数据");
        //如果有就从把从redis中获取的json数据转换成 Map<String, List<Catalogs2Vo>> 类型的对象返回
        Map<String, List<Catalogs2Vo>> map = JSON.parseObject(catalogJSON, new TypeReference<Map<String, List<Catalogs2Vo>>>() {
        });
        return map;
    }


    public Map<String, List<Catalogs2Vo>> getCatalogJsonFromDBWithRedissonLock() {

        //加锁 锁的名字 锁的粒度  越细越快
        RLock lock = redissonClient.getLock("catalogJson-lock");
        lock.lock();

        Map<String, List<Catalogs2Vo>> catelogData;
        try {
            catelogData = getCatelogData();
        } finally {
            //解锁
            lock.unlock();
        }

        return catelogData;

    }


    /**
     * 使用redis的锁nx ex机制设置锁
     *
     * @return
     */
    public Map<String, List<Catalogs2Vo>> getCatalogJsonFromDBWithRedisLock() {


        //分布式锁 去redis占坑
        String uuid = UUID.randomUUID().toString();
        //加锁 和 设置过期时间 保持原子性
        Boolean lock = stringRedisTemplate.opsForValue().setIfAbsent("lock", uuid, 300, TimeUnit.SECONDS);
        if (lock) {
            //如果存在执行业务
            Map<String, List<Catalogs2Vo>> catelogData = null;
            try {
                catelogData = getCatelogData();
            } finally {
                //校验 + 校验成功释放锁 保持原子性
                String script = "if redis.call('get',KEYS[1]) == ARGV[1] then return redis.call('del',KEYS[1]) else return 0 end";
                Long lockResult = stringRedisTemplate.execute(new DefaultRedisScript<Long>(script, Long.class),
                        Arrays.asList("lock"), uuid);
            }

            return catelogData;
        } else {
            //如果拿不到分布式锁就自旋
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return getCatalogJsonFromDBWithRedisLock();
        }


    }


    /**
     * 使用本地锁
     *
     * @return
     */
    public Map<String, List<Catalogs2Vo>> getCatalogJsonFromDBWithLocal() {

        synchronized (this) {
            String catalogJSON = stringRedisTemplate.opsForValue().get("catalogJSON");
            if (StringUtils.isNotBlank(catalogJSON)) {
                return JSON.parseObject(catalogJSON, new TypeReference<Map<String, List<Catalogs2Vo>>>() {
                });
            }
            return getCatelogData();
        }
    }

    private Map<String, List<Catalogs2Vo>> getCatelogData() {

        //有一些进程竞争锁的时候，由于还没有缓存，但是他竟争完以后就有缓存了，他并不是去查缓存，而是拿到锁以后会查询数据库
        //所以应该得到锁以后再去缓存中确定一次，如果没有才继续查

        String catalogJSON = stringRedisTemplate.opsForValue().get("catalogJSON");
        if (StringUtils.isNotBlank(catalogJSON)) {
            return JSON.parseObject(catalogJSON, new TypeReference<Map<String, List<Catalogs2Vo>>>() {
            });
        }

        System.out.println("查询数据库....");
        // 性能优化：将数据库的多次查询变为一次
        List<CategoryEntity> selectList = this.baseMapper.selectList(null);

        //1、查出所有分类
        //1、1）查出所有一级分类
        List<CategoryEntity> level1Categories = getParentCid(selectList, 0L);

        //封装数据
        Map<String, List<Catalogs2Vo>> parentCid = level1Categories.stream().collect(Collectors.toMap(k -> k.getCatId().toString(), v -> {
            //1、每一个的一级分类,查到这个一级分类的二级分类
            List<CategoryEntity> categoryEntities = getParentCid(selectList, v.getCatId());

            //2、封装上面的结果
            List<Catalogs2Vo> catalogs2Vos = null;
            if (categoryEntities != null) {
                catalogs2Vos = categoryEntities.stream().map(l2 -> {
                    Catalogs2Vo catalogs2Vo = new Catalogs2Vo(v.getCatId().toString(), null, l2.getCatId().toString(), l2.getName().toString());

                    //1、找当前二级分类的三级分类封装成vo
                    List<CategoryEntity> level3Catelog = getParentCid(selectList, l2.getCatId());

                    if (level3Catelog != null) {
                        List<Catalogs2Vo.Category3Vo> category3Vos = level3Catelog.stream().map(l3 -> {
                            //2、封装成指定格式
                            Catalogs2Vo.Category3Vo category3Vo = new Catalogs2Vo.Category3Vo(l2.getCatId().toString(), l3.getCatId().toString(), l3.getName());

                            return category3Vo;
                        }).collect(Collectors.toList());
                        catalogs2Vo.setCatalog3List(category3Vos);
                    }

                    return catalogs2Vo;
                }).collect(Collectors.toList());
            }

            return catalogs2Vos;
        }));

        String s = JSON.toJSONString(parentCid);
        stringRedisTemplate.opsForValue().set("catalogJSON", s, 1, TimeUnit.DAYS);

        return parentCid;
    }

    private List<CategoryEntity> getParentCid(List<CategoryEntity> selectList, Long parentCid) {
        return selectList.stream().filter(item -> item.getParentCid().equals(parentCid)).collect(Collectors.toList());
    }




}