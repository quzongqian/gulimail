package com.qzq.gulimall.product.app;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.qzq.common.utils.PageUtils;
import com.qzq.gulimall.product.entity.ProductAttrValueEntity;
import com.qzq.gulimall.product.service.ProductAttrValueService;
import com.qzq.gulimall.product.vo.AttrRespVo;
import com.qzq.gulimall.product.vo.AttrVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.qzq.gulimall.product.entity.AttrEntity;
import com.qzq.gulimall.product.service.AttrService;
import com.qzq.common.utils.R;

import javax.annotation.Resource;


/**
 * 商品属性
 *
 * @author quzongqian
 * @email 2959720739@qq.com
 * @date 2023-03-19 16:29:46
 */
@RestController
@RequestMapping("product/attr")
public class AttrController {


    @Autowired
    private AttrService attrService;


    @Resource
    private ProductAttrValueService productAttrValueService;
    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("product:attr:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = attrService.queryPage(params);

        return R.ok().put("page", page);
    }

    /**
     * 规格维护 回显
     * @param spuId
     * @return
     */
    ///product/attr/base/listforspu/{spuId}
    @GetMapping("/base/listforspu/{spuId}")
    public R list (@PathVariable("spuId") Long spuId){
        List<ProductAttrValueEntity> productAttrValueEntities = productAttrValueService.listProductAttrValue(spuId);
        return R.ok().put("data", productAttrValueEntities);
    }

    /**
     * 修改规格维护
     * @param spuId
     * @param productAttrValueEntitys
     * @return
     */
    ///product/attr/update/{spuId}
    @PostMapping("/update/{spuId}")
    public R update(@PathVariable("spuId") Long spuId,
                    @RequestBody List<ProductAttrValueEntity> productAttrValueEntitys){

        productAttrValueService.updateProductAttrValue(spuId, productAttrValueEntitys);
        return R.ok();
    }




    /**
     * 分页查询 基本属性/销售属性 详情
     * @param params 参数map
     * @param catelogId 分类id
     * @Param type 销售类型 1表示基本类型 0表示销售类型
     * @return
     */
//    /product/attr/sale/list/{catelogId}
//    /product/attr/base/list/{catelogId}
    @GetMapping("/{attrType}/list/{catelogId}")
    public R list(@RequestParam Map<String, Object> params,
                  @PathVariable("catelogId") Long catelogId,
                  @PathVariable("attrType") String type){
        PageUtils page = attrService.queryDetailPage(params, catelogId, type);
        return R.ok().put("page", page);
    }

    /**
     *  查询 修改的属性详情
     * @param attrId 属性id
     * @return
     */
//    /product/attr/info/{attrId}
    @GetMapping("/info/{attrId}")
    public R lst(@PathVariable("attrId") Long attrId){
        AttrRespVo respVos = attrService.getUpdateAttrById(attrId);
        return R.ok().put("attr", respVos);
    }

    /**
     * 修改属性值
     * @param attr
     * @return
     */
//    /product/attr/update
    @PostMapping("/update")
    public R update(@RequestBody AttrVo attr){
        attrService.updateAttr(attr);
        return R.ok();
    }



    /**
     * 信息
     */
    @RequestMapping("/info/{attrId}")
    //@RequiresPermissions("product:attr:info")
    public R info(@PathVariable("attrId") Long attrId){
		AttrEntity attr = attrService.getById(attrId);

        return R.ok().put("attr", attr);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("product:attr:save")
    public R save(@RequestBody AttrVo attr){
		attrService.saveAttr(attr);

        return R.ok();
    }



    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("product:attr:delete")
    public R delete(@RequestBody Long[] attrIds){
		attrService.removeByIds(Arrays.asList(attrIds));

        return R.ok();
    }

}
