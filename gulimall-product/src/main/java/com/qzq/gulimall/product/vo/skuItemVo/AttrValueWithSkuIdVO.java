package com.qzq.gulimall.product.vo.skuItemVo;

import lombok.Data;

@Data
    public class AttrValueWithSkuIdVO {

        private String attrValue;

        private String skuIds;

    }
