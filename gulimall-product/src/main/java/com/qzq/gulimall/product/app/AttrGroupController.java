package com.qzq.gulimall.product.app;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.qzq.gulimall.product.entity.AttrEntity;
import com.qzq.gulimall.product.service.AttrAttrgroupRelationService;
import com.qzq.gulimall.product.service.AttrService;
import com.qzq.gulimall.product.service.CategoryService;
import com.qzq.gulimall.product.vo.AttrGroupRelationVo;
import com.qzq.gulimall.product.vo.GroupAttrsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.qzq.gulimall.product.entity.AttrGroupEntity;
import com.qzq.gulimall.product.service.AttrGroupService;
import com.qzq.common.utils.PageUtils;
import com.qzq.common.utils.R;

import javax.annotation.Resource;


/**
 * 属性分组
 *
 * @author quzongqian
 * @email 2959720739@qq.com
 * @date 2023-03-19 16:29:46
 */
@RestController
@RequestMapping("product/attrgroup")
public class AttrGroupController {
    @Autowired
    private AttrGroupService attrGroupService;

    @Autowired
    private CategoryService categoryService;

    @Resource
    private AttrService attrService;

    @Resource
    private AttrAttrgroupRelationService attrAttrgroupRelationService;


//    /product/attrgroup/{catelogId}/withattr
    @GetMapping("/{catelogId}/withattr")
    public R getAttrsWithCatelogId(@PathVariable("catelogId") Long catelogId){
        List<GroupAttrsVo> groupAttrsVos = attrGroupService.getAttrsByCateLogId(catelogId);
        return R.ok().put("data", groupAttrsVos);
    }



//    /product/attrgroup/attr/relation
    /**
     * 手动添加关联关系
     * @param AGVo
     * @return
     */
    @PostMapping("/attr/relation")
    public R saveAGRelation(@RequestBody List<AttrGroupRelationVo> AGVo){
        attrAttrgroupRelationService.saveAGRelation(AGVo);
        return R.ok();
    }

    /**
     * 在分组关联属性 查找未被关联的基本属性
     */
//    /product/attrgroup/{attrgroupId}/noattr/relation
    @GetMapping("/{attrgroupId}/noattr/relation")
    public R noAttr(@PathVariable("attrgroupId") Long attrgroupId,
                    @RequestParam Map<String, Object> params){
        PageUtils noAttrInGroup = attrGroupService.getNoAttrInGroup(params, attrgroupId);
        return R.ok().put("page", noAttrInGroup);
    }

    /**
     * 删除属性和分组关系
     * @param agVo
     * @return
     */
//    /product/attrgroup/attr/relation/delete
    @PostMapping("/attr/relation/delete")
    public R delAttrAndGroupRelation(@RequestBody AttrGroupRelationVo[] agVo){

        attrGroupService.delAttrAndGroupRelation(agVo);
        return R.ok();

    }


    //    /product/attrgroup/{attrgroupId}/attr/relation
    @GetMapping("/{attrgroupId}/attr/relation")
    public R getAttrsByGroupId(@PathVariable("attrgroupId") Long attrgroupId){
        List<AttrEntity> entities = attrService.getAttrsByGroupId(attrgroupId);
        return R.ok().put("data", entities);
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("product:attrgroup:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = attrGroupService.queryPage(params);

        return R.ok().put("page", page);
    }

    /**
     * 列表
     */
    @RequestMapping("/list/{catelogId}")
    //@RequiresPermissions("product:attrgroup:list")
    public R list(@RequestParam Map<String, Object> params, @PathVariable("catelogId") Long catelogId){
        //PageUtils page = attrGroupService.queryPage(params);
        PageUtils page = attrGroupService.queryPage(params, catelogId);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{attrGroupId}")
    //@RequiresPermissions("product:attrgroup:info")
    public R info(@PathVariable("attrGroupId") Long attrGroupId){
		AttrGroupEntity attrGroup = attrGroupService.getById(attrGroupId);

        Long catelogId = attrGroup.getCatelogId();
        Long[] ParentCids = categoryService.findParentCidByCatelogId(catelogId);
        attrGroup.setCatelogPath(ParentCids);

        return R.ok().put("attrGroup", attrGroup);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("product:attrgroup:save")
    public R save(@RequestBody AttrGroupEntity attrGroup){
		attrGroupService.save(attrGroup);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("product:attrgroup:update")
    public R update(@RequestBody AttrGroupEntity attrGroup){
		attrGroupService.updateById(attrGroup);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("product:attrgroup:delete")
    public R delete(@RequestBody Long[] attrGroupIds){
		attrGroupService.removeByIds(Arrays.asList(attrGroupIds));

        return R.ok();
    }

}
