package com.qzq.gulimall.product.vo;

import lombok.Data;

@Data
public class CategoryBrandVo {
//    	"brandId": 0,
//		"brandName": "string",
    private Long brandId;
    private String brandName;
}
