package com.qzq.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qzq.common.utils.PageUtils;
import com.qzq.gulimall.product.entity.CategoryEntity;
import com.qzq.gulimall.product.vo.Catalogs2Vo;

import java.util.List;
import java.util.Map;

/**
 * 商品三级分类
 *
 * @author quzongqian
 * @email 2959720739@qq.com
 * @date 2023-03-19 16:29:45
 */
public interface CategoryService extends IService<CategoryEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<CategoryEntity> listWithTree();

    void removeMenusByIds(List<Long> asList);

    Long[] findParentCidByCatelogId(Long catelogId);

    void updateCascade(CategoryEntity category);

    List<CategoryEntity> getCatLevel1();

    Map<String, List<Catalogs2Vo>> getCatalogJson();
}

