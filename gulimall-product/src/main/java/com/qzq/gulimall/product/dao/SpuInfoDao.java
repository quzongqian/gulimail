package com.qzq.gulimall.product.dao;

import com.qzq.gulimall.product.entity.ProductAttrValueEntity;
import com.qzq.gulimall.product.entity.SpuInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * spu信息
 * 
 * @author quzongqian
 * @email 2959720739@qq.com
 * @date 2023-03-19 16:29:45
 */
@Mapper
public interface SpuInfoDao extends BaseMapper<SpuInfoEntity> {


    List<Long> getSearchAttrIds(@Param("attrIds") List<Long> attrIds);

    void updateSpuStatus(@Param("spuId") Long spuId, @Param("code") int code);
}
