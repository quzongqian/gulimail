package com.qzq.gulimall.product.vo;

import com.qzq.gulimall.product.entity.AttrEntity;
import com.qzq.gulimall.product.entity.AttrGroupEntity;
import lombok.Data;

import java.util.List;

@Data
public class GroupAttrsVo extends AttrGroupEntity {
    private List<AttrEntity> attrs;
}
