package com.qzq.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qzq.common.utils.PageUtils;
import com.qzq.gulimall.product.entity.SpuInfoEntity;
import com.qzq.gulimall.product.vo.spusavevo.SpuSaveVo;

import java.util.Map;

/**
 * spu信息
 *
 * @author quzongqian
 * @email 2959720739@qq.com
 * @date 2023-03-19 16:29:45
 */
public interface SpuInfoService extends IService<SpuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveSpuVo(SpuSaveVo saveVo);

    void saveSpuInfo(SpuInfoEntity spuInfoEntity);

    PageUtils queryPageList(Map<String, Object> params);

    void up(Long spuId);

    SpuInfoEntity getSpuInfoBySkuId(Long skuId);
}

