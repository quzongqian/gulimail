package com.qzq.gulimall.product.dao;

import com.qzq.gulimall.product.entity.SpuImagesEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu图片
 * 
 * @author quzongqian
 * @email 2959720739@qq.com
 * @date 2023-03-19 16:29:46
 */
@Mapper
public interface SpuImagesDao extends BaseMapper<SpuImagesEntity> {
	
}
