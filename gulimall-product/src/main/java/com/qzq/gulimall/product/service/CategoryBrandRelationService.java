package com.qzq.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qzq.common.utils.PageUtils;
import com.qzq.gulimall.product.entity.BrandEntity;
import com.qzq.gulimall.product.entity.CategoryBrandRelationEntity;

import java.util.List;
import java.util.Map;

/**
 * 品牌分类关联
 *
 * @author quzongqian
 * @email 2959720739@qq.com
 * @date 2023-03-19 16:29:45
 */
public interface CategoryBrandRelationService extends IService<CategoryBrandRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void detailSave(CategoryBrandRelationEntity categoryBrandRelation);

    void updateDetail(Long brandId, String name);

    void casecadeUpdate(Long catId, String name);

    List<BrandEntity> brandByCatId(Long catId);
}

