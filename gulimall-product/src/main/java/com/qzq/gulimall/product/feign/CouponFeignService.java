package com.qzq.gulimall.product.feign;

import com.qzq.common.to.SkuReductionTo;
import com.qzq.common.to.SpuBounds;
import com.qzq.common.utils.R;
import org.apache.ibatis.annotations.ResultMap;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

@FeignClient("gulimall-coupon")
public interface CouponFeignService {

    @RequestMapping("/coupon/spubounds/save")
    R saveSpuBounds(@RequestBody SpuBounds spuBounds);


    @PostMapping("/coupon/skufullreduction/saveinfo")
    R saveSkuInfo(@RequestBody SkuReductionTo skuReductionTo);
}
