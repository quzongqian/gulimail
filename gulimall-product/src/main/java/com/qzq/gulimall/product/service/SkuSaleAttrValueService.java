package com.qzq.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qzq.common.utils.PageUtils;
import com.qzq.gulimall.product.entity.SkuSaleAttrValueEntity;
import com.qzq.gulimall.product.vo.skuItemVo.SkuItemSaleAttrVo;

import java.util.List;
import java.util.Map;

/**
 * sku销售属性&值
 *
 * @author quzongqian
 * @email 2959720739@qq.com
 * @date 2023-03-19 16:29:46
 */
public interface SkuSaleAttrValueService extends IService<SkuSaleAttrValueEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveSkuAsleAttrValue(List<SkuSaleAttrValueEntity> skuSaleAttrValueEntities);

    List<SkuItemSaleAttrVo> getSaleAttrsBySpuId(Long spuId);

    List<String> getSkuAttrValuesBySkuId(Long skuId);
}

