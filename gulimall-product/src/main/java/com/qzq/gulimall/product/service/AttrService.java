package com.qzq.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qzq.common.utils.PageUtils;
import com.qzq.gulimall.product.entity.AttrEntity;
import com.qzq.gulimall.product.vo.AttrRespVo;
import com.qzq.gulimall.product.vo.AttrVo;

import java.util.List;
import java.util.Map;

/**
 * 商品属性
 *
 * @author quzongqian
 * @email 2959720739@qq.com
 * @date 2023-03-19 16:29:46
 */
public interface AttrService extends IService<AttrEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveAttr(AttrVo attr);

    PageUtils queryDetailPage(Map<String, Object> params, Long catelogId, String type);

    AttrRespVo getUpdateAttrById(Long attrId);

    void updateAttr(AttrVo attr);

    List<AttrEntity> getAttrsByGroupId(Long attrgroupId);
}

