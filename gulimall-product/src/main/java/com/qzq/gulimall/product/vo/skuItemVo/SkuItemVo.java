package com.qzq.gulimall.product.vo.skuItemVo;

import com.qzq.gulimall.product.entity.SkuImagesEntity;
import com.qzq.gulimall.product.entity.SkuInfoEntity;
import com.qzq.gulimall.product.entity.SpuInfoDescEntity;
import com.qzq.gulimall.product.vo.SeckillInfoVo;
import com.qzq.gulimall.product.vo.spusavevo.Attr;
import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.List;

@ToString
@Data
public class SkuItemVo {

    //1、sku基本信息的获取  pms_sku_info
    private SkuInfoEntity info;

    private boolean hasStock = true;

    //2、sku的图片信息    pms_sku_images
    private List<SkuImagesEntity> images;

    //3、获取spu的销售属性组合
    private List<SkuItemSaleAttrVo> saleAttr;

    //4、获取spu的介绍
    private SpuInfoDescEntity desc;

    //5、获取spu的规格参数信息
    private List<SpuItemAttrGroupVo> groupAttrs;


    SeckillInfoVo seckillInfo;//当前商品的秒杀优惠信息




}
