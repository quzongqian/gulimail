package com.qzq.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qzq.common.utils.PageUtils;
import com.qzq.gulimall.product.entity.AttrGroupEntity;
import com.qzq.gulimall.product.vo.AttrGroupRelationVo;
import com.qzq.gulimall.product.vo.GroupAttrsVo;
import com.qzq.gulimall.product.vo.skuItemVo.SpuItemAttrGroupVo;

import java.util.List;
import java.util.Map;

/**
 * 属性分组
 *
 * @author quzongqian
 * @email 2959720739@qq.com
 * @date 2023-03-19 16:29:46
 */
public interface AttrGroupService extends IService<AttrGroupEntity> {

    void delAttrAndGroupRelation(AttrGroupRelationVo[] agVo);

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPage(Map<String, Object> params, Long catelogId);

    PageUtils getNoAttrInGroup(Map<String, Object> params, Long attrgroupId);

    List<GroupAttrsVo> getAttrsByCateLogId(Long catelogId);

    List<SpuItemAttrGroupVo> getAttrGroutWithAttrsBySpuId(Long spuId, Long catalogId);
}

