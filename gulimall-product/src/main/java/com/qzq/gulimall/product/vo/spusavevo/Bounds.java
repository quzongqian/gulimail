/**
  * Copyright 2023 json.cn 
  */
package com.qzq.gulimall.product.vo.spusavevo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Auto-generated: 2023-03-30 9:21:41
 *
 * @author json.cn (i@json.cn)
 * @website http://www.json.cn/java2pojo/
 */
@Data
public class Bounds {

    private BigDecimal buyBounds;
    private BigDecimal growBounds;

}