package com.qzq.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qzq.common.utils.PageUtils;
import com.qzq.gulimall.product.entity.ProductAttrValueEntity;

import java.util.List;
import java.util.Map;

/**
 * spu属性值
 *
 * @author quzongqian
 * @email 2959720739@qq.com
 * @date 2023-03-19 16:29:46
 */
public interface ProductAttrValueService extends IService<ProductAttrValueEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveSpuWithSkuInfo(List<ProductAttrValueEntity> collect);

    List<ProductAttrValueEntity> listProductAttrValue(Long spuId);

    void updateProductAttrValue(Long spuId, List<ProductAttrValueEntity> productAttrValueEntitys);
}

