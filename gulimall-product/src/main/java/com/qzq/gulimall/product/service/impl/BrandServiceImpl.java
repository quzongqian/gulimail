package com.qzq.gulimall.product.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qzq.common.utils.PageUtils;
import com.qzq.common.utils.Query;

import com.qzq.gulimall.product.dao.BrandDao;
import com.qzq.gulimall.product.entity.BrandEntity;
import com.qzq.gulimall.product.service.BrandService;


@Service("brandService")
public class BrandServiceImpl extends ServiceImpl<BrandDao, BrandEntity> implements BrandService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {

        LambdaQueryWrapper<BrandEntity> brandEntityLambdaQueryWrapper = new LambdaQueryWrapper<>();
        String key = (String) params.get("key");
        if (StringUtils.isNotBlank(key)){
            brandEntityLambdaQueryWrapper.eq(BrandEntity::getBrandId, key).or().like(BrandEntity::getName, key);
        }

        IPage<BrandEntity> page = this.page(
                new Query<BrandEntity>().getPage(params),
                brandEntityLambdaQueryWrapper
        );

        return new PageUtils(page);
    }

    @Override
    public List<BrandEntity> getBrandByIds(List<Long> brandIds) {
        LambdaQueryWrapper<BrandEntity> brandEntityLambdaQueryWrapper = new LambdaQueryWrapper<>();
        brandEntityLambdaQueryWrapper.in(BrandEntity::getBrandId, brandIds);
        List<BrandEntity> list = this.list(brandEntityLambdaQueryWrapper);
        return list;
    }

}