package com.qzq.gulimall.product.feign.fallback;

import com.qzq.common.exception.BizCodeEnums;
import com.qzq.common.utils.R;
import com.qzq.gulimall.product.feign.SeckillFeignService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class SeckillFeignServiceFallBack implements SeckillFeignService {
    @Override
    public R getSkuSeckillInfo(Long skuId) {
        log.info("熔断方法调用。。。。。SeckillFeignServiceFallBack");
        return R.error(BizCodeEnums.TOO_MANY_REQUEST.getCode(), BizCodeEnums.TOO_MANY_REQUEST.getMsg());
    }
}
