package com.qzq.gulimall.product.dao;

import com.qzq.gulimall.product.entity.SpuInfoDescEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu信息介绍
 * 
 * @author quzongqian
 * @email 2959720739@qq.com
 * @date 2023-03-19 16:29:45
 */
@Mapper
public interface SpuInfoDescDao extends BaseMapper<SpuInfoDescEntity> {
	
}
