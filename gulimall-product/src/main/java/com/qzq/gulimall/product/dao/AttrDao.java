package com.qzq.gulimall.product.dao;

import com.qzq.gulimall.product.entity.AttrEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品属性
 * 
 * @author quzongqian
 * @email 2959720739@qq.com
 * @date 2023-03-19 16:29:46
 */
@Mapper
public interface AttrDao extends BaseMapper<AttrEntity> {
	
}
