package com.qzq.gulimall.product.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.qzq.common.constant.ProductConstant;
import com.qzq.gulimall.product.dao.AttrAttrgroupRelationDao;
import com.qzq.gulimall.product.dao.AttrDao;
import com.qzq.gulimall.product.dao.CategoryBrandRelationDao;
import com.qzq.gulimall.product.entity.AttrAttrgroupRelationEntity;
import com.qzq.gulimall.product.entity.AttrEntity;
import com.qzq.gulimall.product.service.AttrService;
import com.qzq.gulimall.product.vo.AttrGroupRelationVo;
import com.qzq.gulimall.product.vo.GroupAttrsVo;
import com.qzq.gulimall.product.vo.skuItemVo.SpuItemAttrGroupVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qzq.common.utils.PageUtils;
import com.qzq.common.utils.Query;

import com.qzq.gulimall.product.dao.AttrGroupDao;
import com.qzq.gulimall.product.entity.AttrGroupEntity;
import com.qzq.gulimall.product.service.AttrGroupService;

import javax.annotation.Resource;


@Service("attrGroupService")
public class AttrGroupServiceImpl extends ServiceImpl<AttrGroupDao, AttrGroupEntity> implements AttrGroupService {

    @Resource
    private AttrAttrgroupRelationDao attrAttrgroupRelationDao;

    @Resource
    private CategoryBrandRelationDao categoryBrandRelationDao;

    @Resource
    private AttrDao attrDao;

    @Resource
    private AttrService attrService;

    @Override
    public void delAttrAndGroupRelation(AttrGroupRelationVo[] agVo) {

        List<AttrAttrgroupRelationEntity> attrAttrgroupRelationEntities = Arrays.asList(agVo).stream().map((item) -> {
            AttrAttrgroupRelationEntity attrAttrgroupRelationEntity = new AttrAttrgroupRelationEntity();
            BeanUtils.copyProperties(item, attrAttrgroupRelationEntity);
            return attrAttrgroupRelationEntity;
        }).collect(Collectors.toList());

        attrAttrgroupRelationDao.delAGRelations(attrAttrgroupRelationEntities);

    }

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AttrGroupEntity> page = this.page(
                new Query<AttrGroupEntity>().getPage(params),
                new QueryWrapper<AttrGroupEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params, Long catelogId) {

        LambdaQueryWrapper<AttrGroupEntity> attrGroupEntityLambdaQueryWrapper = new LambdaQueryWrapper<>();

        String key = (String)params.get("key");
        if (StringUtils.isNotBlank(key)){
            //根据key模糊查询
            attrGroupEntityLambdaQueryWrapper.and(obj -> {
                obj.eq(AttrGroupEntity::getAttrGroupId, key).or().like(AttrGroupEntity::getAttrGroupName, key);
            });
        }

        if (catelogId == 0){
            IPage<AttrGroupEntity> page = this.page(new Query<AttrGroupEntity>().getPage(params),
                    attrGroupEntityLambdaQueryWrapper);
            return new PageUtils(page);

        } else {

            //根据商品id确定都有那些分组
            attrGroupEntityLambdaQueryWrapper.eq(AttrGroupEntity::getCatelogId, catelogId);

            IPage<AttrGroupEntity> page = this.page(new Query<AttrGroupEntity>().getPage(params),
                    attrGroupEntityLambdaQueryWrapper);

            return new PageUtils(page);
        }

    }

    @Override
    public PageUtils getNoAttrInGroup(Map<String, Object> params, Long attrgroupId) {

        //查询当前分组对应的分类
        AttrGroupEntity byId = this.getById(attrgroupId);
        Long catelogId = byId.getCatelogId();

        //查询分类下没有绑定分组的属性，并且此属性没有被分组绑定
        // 1. 查询当前分类下的所有分组
        LambdaQueryWrapper<AttrGroupEntity> attrGroupEntityLambdaQueryWrapper = new LambdaQueryWrapper<>();
        attrGroupEntityLambdaQueryWrapper.eq(AttrGroupEntity::getCatelogId, catelogId);
        List<AttrGroupEntity> attrGroupEntities = this.baseMapper.selectList(attrGroupEntityLambdaQueryWrapper);
        //    把这些分组的ip取出来
        List<Object> groupList = attrGroupEntities.stream().map((item) -> {
            Long attrGroupId = item.getAttrGroupId();
            return attrGroupId;
        }).collect(Collectors.toList());

        if (groupList == null){
            return null;
        }
        // 2. 这些分组关联的属性
        LambdaQueryWrapper<AttrAttrgroupRelationEntity> relationEntityLambdaQueryWrapper = new LambdaQueryWrapper<>();
        relationEntityLambdaQueryWrapper.in(AttrAttrgroupRelationEntity::getAttrGroupId, groupList);
        List<AttrAttrgroupRelationEntity> attrAttrgroupRelationEntities = attrAttrgroupRelationDao.selectList(relationEntityLambdaQueryWrapper);
        //  把这些分类id收集起来
        List<Long> attrIdList = attrAttrgroupRelationEntities.stream().map((item) -> {
            return item.getAttrId();
        }).collect(Collectors.toList());

        // 3. 当前分类下移除这些属性
        LambdaQueryWrapper<AttrEntity> wrapper = new LambdaQueryWrapper<AttrEntity>().eq(AttrEntity::getCatelogId, catelogId).eq(AttrEntity::getAttrType, ProductConstant.AttrMenu.ATTR_TYPE_BASE.getCode());
        if(attrIdList != null && attrIdList.size() !=0){
            wrapper.notIn(AttrEntity::getAttrId, attrIdList);
        }
        String key = (String) params.get("key");
        if (StringUtils.isNotBlank(key)){
            wrapper.and((w) ->{
                w.eq(AttrEntity::getAttrId, key).or().like(AttrEntity::getAttrName, key);
            });
        }
        IPage<AttrEntity> page = attrService.page(new Query<AttrEntity>().getPage(params), wrapper);
        PageUtils pageUtils = new PageUtils(page);
        return pageUtils;
    }

    @Override
    public List<GroupAttrsVo> getAttrsByCateLogId(Long catelogId) {
        List<AttrGroupEntity> attrGroupEntities = this.baseMapper.selectList(new LambdaQueryWrapper<AttrGroupEntity>().eq(AttrGroupEntity::getCatelogId, catelogId));
        List<GroupAttrsVo> collect = attrGroupEntities.stream().map(item -> {
            GroupAttrsVo groupAttrsVo = new GroupAttrsVo();
            BeanUtils.copyProperties(item, groupAttrsVo);
            Long attrGroupId = item.getAttrGroupId();
            List<AttrEntity> attrsByGroupId = attrService.getAttrsByGroupId(attrGroupId);
            groupAttrsVo.setAttrs(attrsByGroupId);
            return groupAttrsVo;
        }).collect(Collectors.toList());
        return collect;
    }

    @Override
    public List<SpuItemAttrGroupVo> getAttrGroutWithAttrsBySpuId(Long spuId, Long catalogId) {
        List<SpuItemAttrGroupVo> vo = this.baseMapper.getAttrGroutWithAttrsBySpuId(spuId, catalogId);
        return vo;
    }


}