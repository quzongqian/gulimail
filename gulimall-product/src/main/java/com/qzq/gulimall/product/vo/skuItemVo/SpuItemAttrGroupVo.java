package com.qzq.gulimall.product.vo.skuItemVo;

import com.qzq.gulimall.product.vo.spusavevo.Attr;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
    @ToString
    public class SpuItemAttrGroupVo {

        private String groupName;

        private List<Attr> attrs;

    }