package com.qzq.gulimall.product.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.qzq.common.constant.ProductConstant;
import com.qzq.gulimall.product.dao.AttrAttrgroupRelationDao;
import com.qzq.gulimall.product.dao.AttrGroupDao;
import com.qzq.gulimall.product.dao.CategoryDao;
import com.qzq.gulimall.product.entity.AttrAttrgroupRelationEntity;
import com.qzq.gulimall.product.entity.AttrGroupEntity;
import com.qzq.gulimall.product.entity.CategoryEntity;
import com.qzq.gulimall.product.service.AttrAttrgroupRelationService;
import com.qzq.gulimall.product.service.CategoryService;
import com.qzq.gulimall.product.vo.AttrRespVo;
import com.qzq.gulimall.product.vo.AttrVo;
import org.springframework.beans.BeanUtils;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qzq.common.utils.PageUtils;
import com.qzq.common.utils.Query;

import com.qzq.gulimall.product.dao.AttrDao;
import com.qzq.gulimall.product.entity.AttrEntity;
import com.qzq.gulimall.product.service.AttrService;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


@Service("attrService")
public class AttrServiceImpl extends ServiceImpl<AttrDao, AttrEntity> implements AttrService {

    @Resource
    private AttrAttrgroupRelationService attrAttrgroupRelationService;

    @Resource
    private AttrAttrgroupRelationDao attrAttrgroupRelationDao;

    @Resource
    private AttrGroupDao attrGroupDao;

    @Resource
    private CategoryDao categoryDao;

    @Resource
    private CategoryService categoryService;


    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AttrEntity> page = this.page(
                new Query<AttrEntity>().getPage(params),
                new QueryWrapper<AttrEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    @Transactional
    public void saveAttr(AttrVo attr) {
        //保存自己
        AttrEntity attrEntity = new AttrEntity();
        BeanUtils.copyProperties(attr, attrEntity);
        this.save(attrEntity);

        //保存关联关系表 如果是基本类型就需要保存attr_type=1 如果是销售类型就不需要保存(attr_type=0)
        if (attrEntity.getAttrType() == ProductConstant.AttrMenu.ATTR_TYPE_BASE.getCode() && attr.getAttrGroupId() != null){
            AttrAttrgroupRelationEntity attrAttrgroupRelationEntity = new AttrAttrgroupRelationEntity();
            attrAttrgroupRelationEntity.setAttrId(attrEntity.getAttrId());
            attrAttrgroupRelationEntity.setAttrGroupId(attr.getAttrGroupId());
            attrAttrgroupRelationService.save(attrAttrgroupRelationEntity);
        }

    }

    /**
     * 规格参数分页查询
     * @param params
     * @param catelogId
     * @param type
     * @return
     */
    @Override
    public PageUtils queryDetailPage(Map<String, Object> params, Long catelogId, String type) {
        LambdaQueryWrapper<AttrEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AttrEntity::getAttrType, "base".equalsIgnoreCase(type) ? ProductConstant.AttrMenu.ATTR_TYPE_BASE.getCode() : ProductConstant.AttrMenu.ATTR_TYPE_SLAE.getCode());

        if (catelogId != 0){
            queryWrapper.eq(AttrEntity::getCatelogId, catelogId);
        }

        String key = (String) params.get("key");
        if (StringUtils.isNotBlank(key)){
            queryWrapper.eq(AttrEntity::getAttrId, key).or().like(AttrEntity::getAttrName, key);
        }

        IPage<AttrEntity> page = this.page(
                new Query<AttrEntity>().getPage(params),
                queryWrapper
        );
        PageUtils pageUtils = new PageUtils(page);

        List<AttrEntity> records = page.getRecords();
        List<Object> attrRespVos = records.stream().map(item -> {
            AttrRespVo attrRespVo = new AttrRespVo();
            BeanUtils.copyProperties(item, attrRespVo);

            //查询分组 设置分组名
            //如果是基本属性才设置分组， 销售属性没有分组
            if ("base".equalsIgnoreCase(type)){
                AttrAttrgroupRelationEntity attrAttrgroupRelationEntity = attrAttrgroupRelationDao.selectOne(
                        new LambdaQueryWrapper<AttrAttrgroupRelationEntity>()
                                .eq(AttrAttrgroupRelationEntity::getAttrId, item.getAttrId()));
                if (attrAttrgroupRelationEntity != null && attrAttrgroupRelationEntity.getAttrGroupId() !=null ) {
                    Long attrGroupId = attrAttrgroupRelationEntity.getAttrGroupId();
                    AttrGroupEntity attrGroupEntity = attrGroupDao.selectById(attrGroupId);
                    attrRespVo.setGroupName(attrGroupEntity.getAttrGroupName());
                }
            }


            //查询分类 设置分类名
            CategoryEntity category = categoryDao.selectById(item.getAttrId());
            if (category != null) {
                attrRespVo.setCatelogName(category.getName());
            }
            return attrRespVo;
        }).collect(Collectors.toList());

        pageUtils.setList(attrRespVos);
        return pageUtils;
    }

    @Cacheable(value = "attr", key = "'attrinfo:' + #root.args[0]")
    @Override
    public AttrRespVo getUpdateAttrById(Long attrId) {

        AttrRespVo attrRespVo = new AttrRespVo();
        AttrEntity attr = this.getById(attrId);
        BeanUtils.copyProperties(attr,attrRespVo );

        if (attr.getAttrType() == ProductConstant.AttrMenu.ATTR_TYPE_BASE.getCode()){
            //主要是设置分组id
            AttrAttrgroupRelationEntity attrgroupRelation = attrAttrgroupRelationDao.selectOne(new LambdaQueryWrapper<AttrAttrgroupRelationEntity>()
                    .eq(AttrAttrgroupRelationEntity::getAttrId, attrId));
            if (attrgroupRelation != null){
                AttrGroupEntity attrGroupEntity = attrGroupDao.selectById(attrgroupRelation.getAttrGroupId());
                if (attrGroupEntity != null){
                    attrRespVo.setGroupName(attrGroupEntity.getAttrGroupName());
                    attrRespVo.setAttrGroupId(attrGroupEntity.getAttrGroupId());
                }
            }
        }

        //完整路径
        AttrEntity byId = this.getById(attrId);
        if (byId != null){
            CategoryEntity category = categoryDao.selectById(byId.getCatelogId());
            if (category != null){
                Long[] parentCidByCatelogId = categoryService.findParentCidByCatelogId(category.getCatId());
                attrRespVo.setCatelogPath(parentCidByCatelogId);
                attrRespVo.setCatelogName(category.getName());
            }
        }

        return attrRespVo;
    }

    @Override
    @Transactional
    public void updateAttr(AttrVo attr) {
        
        //先修改自己的表
        AttrEntity attrEntity = new AttrEntity();
        BeanUtils.copyProperties(attr, attrEntity);
        this.updateById(attrEntity);

        //修改关联分组表 基本属性
        if (attrEntity.getAttrType() == ProductConstant.AttrMenu.ATTR_TYPE_BASE.getCode()){
            AttrAttrgroupRelationEntity relationEntity = new AttrAttrgroupRelationEntity();
            relationEntity.setAttrId(attr.getAttrId());
            relationEntity.setAttrGroupId(attr.getAttrGroupId());
            Integer count = attrAttrgroupRelationDao.selectCount(new LambdaQueryWrapper<AttrAttrgroupRelationEntity>().eq(AttrAttrgroupRelationEntity::getAttrGroupId, relationEntity.getAttrGroupId()));

            //判单分组参数有没有值，如果分组参数没有值就说明是新插入，如果有值是修改
            if (count > 0){
                attrAttrgroupRelationDao.update(relationEntity,
                        new LambdaUpdateWrapper<AttrAttrgroupRelationEntity>()
                                .eq(AttrAttrgroupRelationEntity::getAttrId, relationEntity.getAttrId()));
            } else {
                attrAttrgroupRelationDao.insert(relationEntity);
            }

        }


    }

    @Override
    public List<AttrEntity> getAttrsByGroupId(Long attrgroupId) {

        LambdaQueryWrapper<AttrAttrgroupRelationEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AttrAttrgroupRelationEntity::getAttrGroupId, attrgroupId);
        List<AttrAttrgroupRelationEntity> relationEntities = attrAttrgroupRelationDao.selectList(queryWrapper);

        if (relationEntities != null && relationEntities.size() != 0){
            List<AttrEntity> attrEntities = relationEntities.stream().map((item) -> {
                AttrEntity byId = this.getById(item.getAttrId());
                return byId;
            }).collect(Collectors.toList());

            return attrEntities;
        }

        return null;

    }

}