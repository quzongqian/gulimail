package com.qzq.gulimall.product.service.impl;
import java.math.BigDecimal;

import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.qzq.common.constant.ProductConstant;
import com.qzq.common.to.SkuHasStockTo;
import com.qzq.common.to.SkuReductionTo;
import com.qzq.common.to.SpuBounds;
import com.qzq.common.to.es.SkuEsModel;
import com.qzq.common.utils.R;
import com.qzq.gulimall.product.entity.*;
import com.qzq.gulimall.product.feign.CouponFeignService;
import com.qzq.gulimall.product.feign.SearchFeignService;
import com.qzq.gulimall.product.feign.WareFeignService;
import com.qzq.gulimall.product.service.*;
import com.qzq.gulimall.product.vo.spusavevo.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qzq.common.utils.PageUtils;
import com.qzq.common.utils.Query;

import com.qzq.gulimall.product.dao.SpuInfoDao;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


@Slf4j
@Service("spuInfoService")
public class SpuInfoServiceImpl extends ServiceImpl<SpuInfoDao, SpuInfoEntity> implements SpuInfoService {

    @Resource
    private SpuInfoDescService spuInfoDescService;

    @Resource
    private SpuImagesService spuImagesService;

    @Resource
    private AttrService attrService;

    @Resource
    private ProductAttrValueService productAttrValueService;

    @Resource
    private SkuInfoService skuInfoService;

    @Resource
    private SkuImagesService skuImagesService;

    @Resource
    private SkuSaleAttrValueService skuSaleAttrValueService;

    @Resource
    private CouponFeignService couponFeignService;

    @Resource
    private BrandService brandService;

    @Resource
    private CategoryService categoryService;

    @Resource
    private WareFeignService wareFeignService;

    @Resource
    private SearchFeignService searchFeignService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SpuInfoEntity> page = this.page(
                new Query<SpuInfoEntity>().getPage(params),
                new QueryWrapper<SpuInfoEntity>()
        );

        return new PageUtils(page);
    }


    /**
     * 保存添加商品功能
     * TODO 一系列问题高级部分添加解决办法
     * @param saveVo
     */
    @Override
    @Transactional
    public void saveSpuVo(SpuSaveVo saveVo) {

        //1.保存spu基本信息 pms_spu_info
        SpuInfoEntity spuInfoEntity = new SpuInfoEntity();
        BeanUtils.copyProperties(saveVo, spuInfoEntity);
        spuInfoEntity.setCreateTime(new Date());
        spuInfoEntity.setUpdateTime(new Date());
        this.saveSpuInfo(spuInfoEntity);

        //2.保存spu描述图片 pms_spu_info_desc
        List<String> decript = saveVo.getDecript();
        SpuInfoDescEntity spuInfoDescEntity = new SpuInfoDescEntity();
        spuInfoDescEntity.setSpuId(spuInfoEntity.getId());
        spuInfoDescEntity.setDecript(String.join(",", decript));
        spuInfoDescService.saveSpuInfoDecs(spuInfoDescEntity);

        //3.保存spu图片 pms_spu_images
        List<String> images = saveVo.getImages();
        spuImagesService.saveImages(spuInfoEntity.getId(), images);

        //4.保存spu的规格参数 pms_product_attr_value
        List<BaseAttrs> baseAttrs = saveVo.getBaseAttrs();
        List<ProductAttrValueEntity> collect = baseAttrs.stream().map(attr -> {
            ProductAttrValueEntity productAttrValueEntity = new ProductAttrValueEntity();
            productAttrValueEntity.setSpuId(spuInfoEntity.getId());
            productAttrValueEntity.setAttrId(attr.getAttrId());
            AttrEntity attrEntity = attrService.getById(attr.getAttrId());
            productAttrValueEntity.setAttrName(attrEntity.getAttrName());
            productAttrValueEntity.setAttrValue(attr.getAttrValues());
            productAttrValueEntity.setQuickShow(attr.getShowDesc());
            return productAttrValueEntity;
        }).collect(Collectors.toList());
        productAttrValueService.saveSpuWithSkuInfo(collect);
        //5.保存spu的积分信息 gulimall_sms -> sms_spu_bounds
        Bounds bounds = saveVo.getBounds();
        SpuBounds spuBounds = new SpuBounds();
        spuBounds.setSpuId(spuInfoEntity.getId());
        BeanUtils.copyProperties(bounds, spuBounds);
        R r1 = couponFeignService.saveSpuBounds(spuBounds);
        if (r1.getCode() != 0){
            log.error("spu保存失败");
        }

        //6.保存spu的信息  保存当前spu对应sku的信息
        List<Skus> skus = saveVo.getSkus();
        if (skus.size() > 0 || skus != null){

            skus.forEach(sku -> {
                //6.1 保存sku的基本信息 pms_sku_info
                List<Images> imagesList = sku.getImages();
                String defaultImg = "";
                if (imagesList != null || imagesList.size() > 0){
                    for (Images img : imagesList) {
                        if (img.getDefaultImg() == 1){
                            defaultImg = img.getImgUrl();
                        }
                    }
                }
                SkuInfoEntity skuInfoEntity = new SkuInfoEntity();
                BeanUtils.copyProperties(sku, skuInfoEntity);
                skuInfoEntity.setSpuId(spuInfoEntity.getId());
                skuInfoEntity.setCatalogId(spuInfoEntity.getCatalogId());
                skuInfoEntity.setBrandId(spuInfoEntity.getBrandId());
                skuInfoEntity.setSkuDefaultImg(defaultImg);
                skuInfoEntity.setSaleCount(0L);
                skuInfoService.saveSkuInfo(skuInfoEntity);

                //6.2 保存sku的图片信息 pms_sku_images
                List<SkuImagesEntity> imagesEntityList = sku.getImages().stream().map(img -> {
                    SkuImagesEntity skuImagesEntity = new SkuImagesEntity();
                    skuImagesEntity.setSkuId(skuInfoEntity.getSkuId());
                    skuImagesEntity.setDefaultImg(img.getDefaultImg());
                    skuImagesEntity.setImgUrl(img.getImgUrl());
                    return skuImagesEntity;
                }).filter(entity -> {
                    return !StringUtils.isEmpty(entity.getImgUrl());
                }).collect(Collectors.toList());
                //TODO 没有图片；路径不需要保存
                skuImagesService.saveSkuImages(imagesEntityList);

                //6.3 保存sku的销售属性信息 pms_sku_sale_attr_value
                List<Attr> attr = sku.getAttr();
                List<SkuSaleAttrValueEntity> skuSaleAttrValueEntities = attr.stream().map(a -> {
                    SkuSaleAttrValueEntity skuSaleAttrValueEntity = new SkuSaleAttrValueEntity();
                    BeanUtils.copyProperties(a, skuSaleAttrValueEntity);
                    skuSaleAttrValueEntity.setSkuId(skuInfoEntity.getSkuId());
                    return skuSaleAttrValueEntity;
                }).collect(Collectors.toList());
                skuSaleAttrValueService.saveSkuAsleAttrValue(skuSaleAttrValueEntities);

                //6.4 保存sku的优惠 满减信息
                // gulimall_sms -》 sms_sku_ladder(打折表)  sms_sku_full_reduction (满减表) sms_member_price(会员价格表)
                SkuReductionTo skuReductionTo = new SkuReductionTo();
                BeanUtils.copyProperties(sku, skuReductionTo);
                if (skuReductionTo.getFullCount() > 0 || skuReductionTo.getFullPrice().compareTo(new BigDecimal("0")) == 1){
                    R r = couponFeignService.saveSkuInfo(skuReductionTo);
                    if (r.getCode() != 0){
                        log.error("sku保存出错");
                    }
                }
            });
        }





    }

    @Override
    public void saveSpuInfo(SpuInfoEntity spuInfoEntity) {
        this.save(spuInfoEntity);
    }

    @Override
    public PageUtils queryPageList(Map<String, Object> params) {

        LambdaQueryWrapper<SpuInfoEntity> wrapper = new LambdaQueryWrapper<>();
//key: '华为',//检索关键字
//   catelogId: 6,//三级分类id
//   brandId: 1,//品牌id
//   status: 0,//商品状态
        String key = (String) params.get("key");
        if (StringUtils.isNotBlank(key)){
            wrapper.and((w) -> {
                w.eq(SpuInfoEntity::getId, key).or().like(SpuInfoEntity::getSpuName, key);
            });
        }

        String catelogId = (String) params.get("catelogId");
        if (StringUtils.isNotBlank(catelogId) && !"0".equalsIgnoreCase(catelogId)){
            wrapper.eq(SpuInfoEntity::getCatalogId, catelogId);
        }

        String brandId = (String) params.get("brandId");
        if(StringUtils.isNotBlank(brandId) && !"0".equalsIgnoreCase(brandId)){
            wrapper.eq(SpuInfoEntity::getBrandId, brandId);
        }

        String status = (String) params.get("status");
        if (StringUtils.isNotBlank(status)){
            wrapper.eq(SpuInfoEntity::getPublishStatus, status);
        }

        IPage<SpuInfoEntity> page = this.page(
                new Query<SpuInfoEntity>().getPage(params),
                wrapper
        );

        return new PageUtils(page);
    }

    @Override
    public void up(Long spuId) {

        //查询根据spu查询sku
        List<SkuInfoEntity> skuInfoEntities = skuInfoService.getSkuBySpuId(spuId);

        //收集所有skuId
        List<Long> SkuIds = skuInfoEntities.stream().map(SkuInfoEntity::getSkuId).collect(Collectors.toList());
        //查询远程调用
        Map<Long, Boolean> hasStockMap = null;
        try {
            R hasStock = wareFeignService.hasStock(SkuIds);
            hasStockMap = hasStock.getData(new TypeReference<List<SkuHasStockTo>>(){}).stream().collect(Collectors.toMap(SkuHasStockTo::getSkuId, item -> item.getHasStock()));
        } catch (Exception e) {
            log.error("调用远程服务ware查看是否有库存出现异常，异常为:{}", e);
        }
        Map<Long, Boolean> finalHasStockMap = hasStockMap;

        //TODO 查找所有可以用来检索的规格属性信息
        //先查看哪些规格属性可以被检索
        List<ProductAttrValueEntity> productAttrValueEntities = productAttrValueService.listProductAttrValue(spuId);
        List<Long> attrIds = productAttrValueEntities.stream().map(item -> {
            return item.getAttrId();
        }).collect(Collectors.toList());
        List<Long> attrSearchIds = baseMapper.getSearchAttrIds(attrIds);
        Set set = new HashSet<Long>(attrSearchIds);
        //从所有的规格属性中挑选出可以被检索的属性
        List<SkuEsModel.Attrs> attrsList = productAttrValueEntities.stream().filter(item -> {
            return set.contains(item.getAttrId());
        }).map(i -> {
            SkuEsModel.Attrs attrs = new SkuEsModel.Attrs();
            BeanUtils.copyProperties(i, attrs);
            return attrs;
        }).collect(Collectors.toList());


        //组装esModes
        List<SkuEsModel> collect = skuInfoEntities.stream().map(sku -> {
            SkuEsModel skuEsModel = new SkuEsModel();
            BeanUtils.copyProperties(sku, skuEsModel);
            skuEsModel.setSkuPrice(sku.getPrice());
            skuEsModel.setSkuImg(sku.getSkuDefaultImg());
            skuEsModel.setAttrs(attrsList);

            //TODO 发送远程调用 看看是否有库存
            if (finalHasStockMap == null){
                skuEsModel.setHasStock(true);
            } else {
                skuEsModel.setHasStock(finalHasStockMap.get(sku.getSkuId()));
            }

            //TODO 热度评分 默认设置为0
            skuEsModel.setHotScore(0L);

            //查看品牌和分类的名字信息
            BrandEntity brandEntity = brandService.getById(sku.getBrandId());
            skuEsModel.setBrandName(brandEntity.getName());
            skuEsModel.setBrandImg(brandEntity.getLogo());

            CategoryEntity categoryEntity = categoryService.getById(sku.getCatalogId());
            skuEsModel.setCatalogName(categoryEntity.getName());


            return skuEsModel;
        }).collect(Collectors.toList());

        //TODO 将数据给es进行保存
        R r = searchFeignService.productStatusUp(collect);
        if (r.getCode() == 0){
            //远程接口调用成功 修改状态
            baseMapper.updateSpuStatus(spuId, ProductConstant.StatusMenu.SPU_UP.getCode());

        } else {
            //远程接口调用失败 TODO 存在幂等性问题 接口重复调用

        }
    }

    @Override
    public SpuInfoEntity getSpuInfoBySkuId(Long skuId) {
        SkuInfoEntity byId = skuInfoService.getById(skuId);
        Long spuId = byId.getSpuId();
        SpuInfoEntity spuInfoEntity = getById(spuId);
        return spuInfoEntity;
    }

}