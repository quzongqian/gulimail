package com.qzq.gulimall.product.dao;

import com.qzq.gulimall.product.entity.CategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品三级分类
 * 
 * @author quzongqian
 * @email 2959720739@qq.com
 * @date 2023-03-19 16:29:45
 */
@Mapper
public interface CategoryDao extends BaseMapper<CategoryEntity> {
	
}
