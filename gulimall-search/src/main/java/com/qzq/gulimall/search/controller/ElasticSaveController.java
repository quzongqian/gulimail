package com.qzq.gulimall.search.controller;

import com.qzq.common.exception.BizCodeEnums;
import com.qzq.common.to.es.SkuEsModel;
import com.qzq.common.utils.R;
import com.qzq.gulimall.search.service.ProductSaveService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;

@RequestMapping("/search/save")
@RestController
@Slf4j
public class ElasticSaveController {

    @Resource
    private ProductSaveService productSaveService;

    @PostMapping("/product")
    public R productStatusUp(@RequestBody List<SkuEsModel> skuEsModels) throws IOException {

        boolean b = false;
        try {
             b = productSaveService.productStatusUp(skuEsModels);
        } catch (IOException e) {
            log.info("ElasticSaveControllerSave发生异常 e:{}",e);
            return R.error(BizCodeEnums.PRODUCT_UP_EXCEPTION.getCode(), BizCodeEnums.PRODUCT_UP_EXCEPTION.getMsg());
        }

        if (b){
            return R.error(BizCodeEnums.PRODUCT_UP_EXCEPTION.getCode(), BizCodeEnums.PRODUCT_UP_EXCEPTION.getMsg());
        } else  {
            return R.ok();
        }
    }
}
