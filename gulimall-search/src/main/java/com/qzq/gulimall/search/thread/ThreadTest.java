package com.qzq.gulimall.search.thread;

import org.junit.jupiter.api.Test;

import java.util.concurrent.*;

public class ThreadTest {


    private static ExecutorService executor = Executors.newFixedThreadPool(10);


    public static void main(String[] args) throws ExecutionException, InterruptedException {

        System.out.println("main.....start");

        CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
            System.out.println("当前线程：" + Thread.currentThread().getId());
            int i = 10 / 2;
            System.out.println("运行结果" + i);
        }, executor);

//        CompletableFuture<Integer> future1 = CompletableFuture.supplyAsync(() -> {
//            System.out.println("当前线程：" + Thread.currentThread().getId());
//            int i = 10 / 2;
//            System.out.println("运行结果" + i);
//            return i;
//        }, executor);
//        Integer integer = future1.get();
//        System.out.println(integer);

        System.out.println("main.....end");


    }



    @Test
    public void test(){

        /**
         * 工作顺序
         * 1.线程池创建，准备好core数量的核心线程，准备接收任务
         * 1.1 core满了，将再进来的任务放入阻塞队列中，空闲的core就会自己去阻塞队列获取任务执行
         * 1.2 阻塞队列满了，就直接开启新的线程执行，最大只能开到max指定的数量
         * 1.3 如果max满了就用 RejectedExecutionHandler 拒绝任务
         * 1.4 max都执行完成，接下来有很多空闲，在指定的 keepAliveTime 时间以后，就会释放 max-core 这些线程
         */

        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(5,
                200,
                10,
                TimeUnit.SECONDS,
                new LinkedBlockingDeque<>(100000),
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.AbortPolicy());
    }
}
