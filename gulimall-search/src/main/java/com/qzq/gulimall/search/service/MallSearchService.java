package com.qzq.gulimall.search.service;

import com.qzq.gulimall.search.vo.SearchParam;
import com.qzq.gulimall.search.vo.SearchResult;

public interface MallSearchService {
    SearchResult search(SearchParam param);
}
