package com.qzq.gulimall.search.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qzq.common.to.es.SkuEsModel;
import com.qzq.common.utils.R;
import com.qzq.gulimall.search.config.GulimallElasticSearchConfig;
import com.qzq.gulimall.search.constant.ESConstant;
import com.qzq.gulimall.search.feign.ProductFeignService;
import com.qzq.gulimall.search.service.MallSearchService;
import com.qzq.gulimall.search.vo.AttrResponseVo;
import com.qzq.gulimall.search.vo.BrandVo;
import com.qzq.gulimall.search.vo.SearchParam;
import com.qzq.gulimall.search.vo.SearchResult;
import org.apache.commons.lang.StringUtils;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.NestedQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.nested.NestedAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.nested.ParsedNested;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedLongTerms;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedStringTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MallSearchServiceImpl implements MallSearchService {

    @Resource
    private RestHighLevelClient client;

    @Resource
    private ProductFeignService productFeignService;

    @Override
    public SearchResult search(SearchParam param) {

        //构建返回结果
        SearchResult result = null;

        //构建检索请求
        SearchRequest searchRequest = buildSearchRequest(param);

        try {
            //执行检索
            SearchResponse searchResponse = client.search(searchRequest, GulimallElasticSearchConfig.COMMON_OPTIONS);

            //将查询出来结果封装进result
            result = buildSearchResult(searchResponse, param);
        } catch (IOException e) {
            e.printStackTrace();
        }


        return result;
    }

    /**
     * 构建dsl检索语句
     * @param param
     * @return
     */
    private SearchRequest buildSearchRequest(SearchParam param) {

        SearchSourceBuilder builder = new SearchSourceBuilder();
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();

        //查询 模糊匹配，过滤，（属性、分类、品牌、价格区间、库存）
        //must 模糊匹配
        if (StringUtils.isNotBlank(param.getKeyword())){
            boolQueryBuilder.must(QueryBuilders.matchQuery("skuTitle", param.getKeyword()));
        }

        //bool filter 三级分类id查询
        if (param.getCatalog3Id() != null){
            boolQueryBuilder.filter(QueryBuilders.termQuery("catalogId", param.getCatalog3Id()));
        }

        //bool filter 品牌分类id查询
        if (param.getBrandId() != null && param.getBrandId().size() > 0) {
            boolQueryBuilder.filter(QueryBuilders.termsQuery("brandId", param.getBrandId()));
        }

        //bool filter 是否有库存
        if (param.getHasStock() != null){
            boolQueryBuilder.filter(QueryBuilders.termQuery("hasStock", param.getHasStock() == 1));
        }

        //bool filter 按照价格区间进行检索 _500/0_500/500_ 三种形式
        if (StringUtils.isNotBlank(param.getSkuPrice())){
            RangeQueryBuilder rangeQuery = QueryBuilders.rangeQuery("skuPrice");
            String[] s = param.getSkuPrice().split("_");
            if (s.length == 2){
                if (param.getSkuPrice().startsWith("_")){
                    rangeQuery.lte(s[1]);
                } else {
                    rangeQuery.gte(s[0]).lte(s[1]);
                }
            } else if (s.length == 1){

                if (param.getSkuPrice().endsWith("_")){
                    rangeQuery.gte(s[0]);
                }
            }
            boolQueryBuilder.filter(rangeQuery);
        }

        //bool filter 根据attrs检索
        if (param.getAttrs() != null && param.getAttrs().size() > 0){
            for (String attr : param.getAttrs()) {
                BoolQueryBuilder nestedBoolQuery = QueryBuilders.boolQuery();

                String[] s = attr.split("_");
                String attrId = s[0];//属性id
                String[] attrValues = s[1].split(":");//属性值
                nestedBoolQuery.must(QueryBuilders.termQuery("attrs.attrId", attrId));
                nestedBoolQuery.must(QueryBuilders.termsQuery("attrs.attrValue", attrValues));
                //每一个属性都必须生成一个嵌入式查询
                NestedQueryBuilder attrs = QueryBuilders.nestedQuery("attrs", nestedBoolQuery, ScoreMode.None);
                boolQueryBuilder.filter(attrs);
            }
        }

        builder.query(boolQueryBuilder);

        //排序
        if (StringUtils.isNotBlank(param.getSort())){
            String sort = param.getSort();
            String[] s = sort.split("_");
            SortOrder order = "asc".equalsIgnoreCase(s[1]) ? SortOrder.ASC : SortOrder.DESC;
            builder.sort(s[0], order);
        }


        // 分页（PageNum - 1） * PageSize
        builder.from((param.getPageNum() - 1) * ESConstant.PRODUCT_PAGESIZE);
        builder.size(ESConstant.PRODUCT_PAGESIZE);

        //高亮
        if (StringUtils.isNotBlank(param.getKeyword())){

            HighlightBuilder highlightBuilder = new HighlightBuilder();
            highlightBuilder.field("skuTitle");
            highlightBuilder.preTags("<b style='color:red'>");
            highlightBuilder.postTags("</b>");

            builder.highlighter(highlightBuilder);
        }


        //聚合
        //品牌聚合
        TermsAggregationBuilder brand_agg = AggregationBuilders.terms("brand_agg");
        brand_agg.field("brandId").size(50);
        //子聚合 名字和img
        brand_agg.subAggregation(AggregationBuilders.terms("brand_name_agg").field("brandName").size(1));
        brand_agg.subAggregation(AggregationBuilders.terms("brand_img_agg").field("brandImg").size(1));
        builder.aggregation(brand_agg);

        //三级分类聚合
        TermsAggregationBuilder catalog_agg = AggregationBuilders.terms("catalog_agg").field("catalogId").size(50);
        catalog_agg.subAggregation(AggregationBuilders.terms("catalog_name_agg").field("catalogName").size(1));
        builder.aggregation(catalog_agg);

        //根据属性聚合
        NestedAggregationBuilder attr_agg = AggregationBuilders.nested("attr_agg", "attrs");
        TermsAggregationBuilder attr_id_agg = AggregationBuilders.terms("attr_id_agg").field("attrs.attrId").size(50);
        //聚合id
        attr_agg.subAggregation(attr_id_agg);
        //id下连个子聚合attrs.attrName  attrs.attrValue
        attr_id_agg.subAggregation(AggregationBuilders.terms("attr_name_agg").field("attrs.attrName").size(1));
        attr_id_agg.subAggregation(AggregationBuilders.terms("attr_value_agg").field("attrs.attrValue").size(50));
        builder.aggregation(attr_agg);
        String s = builder.toString();
        System.out.println("构建的dsl: " + s);
        SearchRequest searchRequest = new SearchRequest(new String[]{ESConstant.PRODUCT_INDEX}, builder);

        return searchRequest;
    }

    /**
     * 封装需要返回的结果集
     * @param response
     * @return
     */
    private SearchResult buildSearchResult(SearchResponse response, SearchParam param) {
        System.out.println(response);
        SearchResult result = new SearchResult();

        //封装商品信息 List<SkuEsModel> product;
        List<SkuEsModel> skuEsModels = new ArrayList<>();
        if ( response.getHits() != null){
            SearchHit[] hits = response.getHits().getHits();
            for (SearchHit hit : hits) {
                String productAsString = hit.getSourceAsString();
                SkuEsModel skuEsModel = JSON.parseObject(productAsString, SkuEsModel.class);

                //查看高亮
                if(StringUtils.isNotBlank(param.getKeyword())){
                    HighlightField skuTitle = hit.getHighlightFields().get("skuTitle");
                    String string = skuTitle.getFragments()[0].string();
                    skuEsModel.setSkuTitle(string);
                }

                skuEsModels.add(skuEsModel);
            }
        }
        result.setProduct(skuEsModels);

        //当前页码Integer pageNum
        result.setPageNum(param.getPageNum());

        //总记录数Long total
        long total = response.getHits().getTotalHits().value;
        result.setTotal(total);


        //总页码 Integer totalPages
        int totalPages = (int) (total % ESConstant.PRODUCT_PAGESIZE) == 0 ? (int) (total / ESConstant.PRODUCT_PAGESIZE) :
                (int) (total / ESConstant.PRODUCT_PAGESIZE + 1);
        result.setTotalPages(totalPages);

        List<Integer> pageNavs = new ArrayList<>();
        for (Integer i = 1; i <= totalPages; i++) {
            pageNavs.add(i);
        };
        result.setPageNavs(pageNavs);

        //品牌信息 List<BrandVo> brands
        ParsedLongTerms aggregations = response.getAggregations().get("brand_agg");
        List<? extends Terms.Bucket> buckets = aggregations.getBuckets();
        List<SearchResult.BrandVo> brandVos = new ArrayList<>();
        for (Terms.Bucket bucket : buckets) {
            SearchResult.BrandVo brandVo = new SearchResult.BrandVo();
            //获取品牌id
            long brandId = bucket.getKeyAsNumber().longValue();
            brandVo.setBrandId(brandId);
            //获取品牌img
            ParsedStringTerms brand_img_agg = bucket.getAggregations().get("brand_img_agg");
            String brandImg = brand_img_agg.getBuckets().get(0).getKeyAsString();
            brandVo.setBrandImg(brandImg);
            //获取品牌名字
            ParsedStringTerms brand_name_agg = bucket.getAggregations().get("brand_name_agg");
            String brandName = brand_name_agg.getBuckets().get(0).getKeyAsString();
            brandVo.setBrandName(brandName);
            brandVos.add(brandVo);
        }
        result.setBrands(brandVos);


        //分类信息 List<CatalogVo> catalogs
        List<SearchResult.CatalogVo> catalogVos = new ArrayList<>();
        ParsedLongTerms aggregation = response.getAggregations().get("catalog_agg");
        for (Terms.Bucket bucket : aggregation.getBuckets()) {
            SearchResult.CatalogVo catalogVo = new SearchResult.CatalogVo();
            //获取品牌id
            long catalogId = bucket.getKeyAsNumber().longValue();
            catalogVo.setCatalogId(catalogId);
            //获取品牌名字
            ParsedStringTerms catalog_name_agg = bucket.getAggregations().get("catalog_name_agg");
            String catalogName = catalog_name_agg.getBuckets().get(0).getKeyAsString();
            catalogVo.setCatalogName(catalogName);
            catalogVos.add(catalogVo);
        }
        result.setCatalogs(catalogVos);


        //属性信息 List<AttrVo> attrs;
        List<SearchResult.AttrVo> attrVos = new ArrayList<>();
        ParsedNested attr_agg = response.getAggregations().get("attr_agg");
        ParsedLongTerms attr_id_agg = attr_agg.getAggregations().get("attr_id_agg");
        for (Terms.Bucket bucket : attr_id_agg.getBuckets()) {
            SearchResult.AttrVo attrVo = new SearchResult.AttrVo();
            //获取属性id
            long attId = bucket.getKeyAsNumber().longValue();
            attrVo.setAttrId(attId);
            //获得属性名字
            ParsedStringTerms attr_name_agg = bucket.getAggregations().get("attr_name_agg");
            String attrName = attr_name_agg.getBuckets().get(0).getKeyAsString();
            attrVo.setAttrName(attrName);
            //获得属性值
            ParsedStringTerms attr_value_agg = bucket.getAggregations().get("attr_value_agg");
            List<String> attrValues = attr_value_agg.getBuckets().stream().map(item -> {
                return item.getKeyAsString();
            }).collect(Collectors.toList());
            attrVo.setAttrValue(attrValues);
            attrVos.add(attrVo);
        }
        result.setAttrs(attrVos);

        //封装面包屑
        //属性面包屑
        if (param.getAttrs() != null && param.getAttrs().size() > 0) {
            List<SearchResult.NavVo> vavVos = param.getAttrs().stream().map(attr -> {
                SearchResult.NavVo navVo = new SearchResult.NavVo();

                //构建面包屑导航功能
                String[] s = attr.split("_");
                //设置面包屑值
                navVo.setNavValue(s[1]);

                //设置面包屑名字
                R r = productFeignService.attrInfo(Long.parseLong(s[0]));
                result.getAttrIds().add(Long.parseLong(s[0]));
                if (r.getCode() == 0){
                    AttrResponseVo attrData = r.getData("attr", new TypeReference<AttrResponseVo>() {});
                    navVo.setNavName(attrData.getAttrName());
                } else {
                    navVo.setNavName(s[0]);
                }

                //取消了面包屑以后要跳转到哪个地方
                String replace = replaceQueryString(param, attr,"attrs");
                navVo.setLink("http://search.gulimall.com/list.html?" + replace);

                return navVo;
            }).collect(Collectors.toList());
            result.setNavs(vavVos);
        }

        //品牌面包屑
        if (param.getBrandId() != null && param.getBrandId().size() > 0){
            List<SearchResult.NavVo> navs = result.getNavs();
            SearchResult.NavVo navVo = new SearchResult.NavVo();

            navVo.setNavName("品牌");
            R r = productFeignService.infos(param.getBrandId());
            if (r.getCode() == 0){
                List<BrandVo> brands = r.getData("brands", new TypeReference<List<BrandVo>>() {
                });

                StringBuffer stringBuffer = new StringBuffer();
                String replace = null;
                for (BrandVo brand : brands) {
                    String brandName = brand.getName();
                    stringBuffer.append(brandName + "、");
                    replace = replaceQueryString(param, brand.getBrandId() + "", "brandId");
                }
                String s = stringBuffer.deleteCharAt(stringBuffer.length() - 1).toString();
                navVo.setNavValue(s);
                navVo.setLink("http://search.gulimall.com/list.html?" + replace);

            }
            navs.add(navVo);
        }


        return result;
    }

    private String replaceQueryString(SearchParam param, String value, String key) {
        String encode = null;
        try {
             encode = URLEncoder.encode(value, "UTF-8");
             encode.replace("+", "%20");//浏览器对空格的编码和java不一样
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String replace = param.get_queryString().replace("&" + key + "=" + encode, "");
        return replace;
    }
}
