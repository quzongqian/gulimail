package com.qzq.gulimall.member.Exception;

public class UserNameExistException extends RuntimeException{
    public UserNameExistException() {
        super("用户名已经存在");
    }
}
