package com.qzq.gulimall.member.dao;

import com.qzq.gulimall.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 * 
 * @author quzongqian
 * @email 2959720739@qq.com
 * @date 2023-03-20 15:22:45
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {
	
}
