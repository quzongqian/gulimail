package com.qzq.gulimall.member.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.http.HttpUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.qzq.gulimall.member.Exception.PhoneExistException;
import com.qzq.gulimall.member.Exception.UserNameExistException;
import com.qzq.gulimall.member.dao.MemberLevelDao;
import com.qzq.gulimall.member.entity.MemberLevelEntity;
import com.qzq.gulimall.member.utils.HttpUtils;
import com.qzq.gulimall.member.vo.MemberLoginVo;
import com.qzq.gulimall.member.vo.MemberRegistVo;
import com.qzq.gulimall.member.vo.SocialUserVo;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qzq.common.utils.PageUtils;
import com.qzq.common.utils.Query;

import com.qzq.gulimall.member.dao.MemberDao;
import com.qzq.gulimall.member.entity.MemberEntity;
import com.qzq.gulimall.member.service.MemberService;

import javax.annotation.Resource;


@Service("memberService")
public class MemberServiceImpl extends ServiceImpl<MemberDao, MemberEntity> implements MemberService {

    @Resource
    private MemberLevelDao memberLevelDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<MemberEntity> page = this.page(
                new Query<MemberEntity>().getPage(params),
                new QueryWrapper<MemberEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void register(MemberRegistVo vo) {

        MemberEntity memberEntity = new MemberEntity();
        //判断会员等级
        MemberLevelEntity memberLevelEntity = memberLevelDao.queryDefaultMemberLevle();
        memberEntity.setLevelId(memberLevelEntity.getId());
        //添加手机号和用户名信息 判断是否位唯一
        checkPhoneUnique(vo.getPhone());
        checkUserNameUnique(vo.getPhone());
        memberEntity.setUsername(vo.getUserName());
        memberEntity.setMobile(vo.getPhone());
        //密码MD5盐值加密
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String encode = bCryptPasswordEncoder.encode(vo.getPassword());
        memberEntity.setPassword(encode);

        this.baseMapper.insert(memberEntity);
    }

    @Override
    public void checkPhoneUnique(String phone) throws PhoneExistException{
        Integer phoneCount = this.baseMapper.selectCount(new LambdaQueryWrapper<MemberEntity>().eq(MemberEntity::getMobile, phone));
        if (phoneCount > 0){
            throw new PhoneExistException();
        }
    }

    @Override
    public void checkUserNameUnique(String userName) throws UserNameExistException{
        Integer integer = this.baseMapper.selectCount(new LambdaQueryWrapper<MemberEntity>().eq(MemberEntity::getUsername, userName));
        if (integer > 0){
            throw new UserNameExistException();
        }
    }

    @Override
    public MemberEntity login(MemberLoginVo vo) {

        String loginacct = vo.getLoginacct();
        String password = vo.getPassword();

        //查找用户根据 用户名或者手机号
        MemberEntity memberEntity = this.baseMapper.selectOne(new LambdaQueryWrapper<MemberEntity>()
                .eq(MemberEntity::getUsername, loginacct).or()
                .eq(MemberEntity::getMobile, loginacct));

        if (memberEntity != null){
            //校验密码
            BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
            boolean matches = bCryptPasswordEncoder.matches(password, memberEntity.getPassword());
            if (matches){
                return memberEntity;
            } else {
                return null;
            }

        } else {
            return null;
        }
    }

    @Override
    public MemberEntity login(SocialUserVo vo) throws Exception {

        //先查询socialUid
        Map<String, String> map = new HashMap<>();
        map.put("access_token", vo.getAccess_token());
        HttpResponse response = HttpUtils.doGet("https://gitee.com", "/api/v5/user", "get", new HashMap<String, String>(), map);

        String s = EntityUtils.toString(response.getEntity());
        JSONObject jsonObject = JSON.parseObject(s);
        String id = jsonObject.get("id").toString();
        String name = jsonObject.get("name").toString();


        MemberEntity memberEntity = this.baseMapper.selectOne(new LambdaQueryWrapper<MemberEntity>().eq(MemberEntity::getSocialUid, id));
        if (memberEntity != null){
            //如果存在就是登录直接返回用户
            MemberEntity updateMember = new MemberEntity();
            updateMember.setId(memberEntity.getId());
            updateMember.setAccessToken(vo.getAccess_token());
            updateMember.setExpiresIn(vo.getExpires_in());
            this.baseMapper.updateById(updateMember);
            memberEntity.setExpiresIn(vo.getExpires_in());
            memberEntity.setAccessToken(vo.getAccess_token());
            return memberEntity;
        } else {
            //如果不存在就是新用户 创建用户
            MemberEntity newMember = new MemberEntity();
            newMember.setUsername(name);
            newMember.setSocialUid(id);
            newMember.setExpiresIn(vo.getExpires_in());
            newMember.setAccessToken(vo.getAccess_token());
            this.baseMapper.insert(newMember);
            return newMember;
        }

    }

}