package com.qzq.gulimall.member.controller;

import java.util.Arrays;
import java.util.Map;

//import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.qzq.common.exception.BizCodeEnums;
import com.qzq.gulimall.member.Exception.PhoneExistException;
import com.qzq.gulimall.member.Exception.UserNameExistException;
import com.qzq.gulimall.member.feign.CouponFeignService;
import com.qzq.gulimall.member.vo.MemberLoginVo;
import com.qzq.gulimall.member.vo.MemberRegistVo;
import com.qzq.gulimall.member.vo.SocialUserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.qzq.gulimall.member.entity.MemberEntity;
import com.qzq.gulimall.member.service.MemberService;
import com.qzq.common.utils.PageUtils;
import com.qzq.common.utils.R;


/**
 * 会员
 *
 * @author quzongqian
 * @email 2959720739@qq.com
 * @date 2023-03-20 15:22:45
 */
@RestController
@RequestMapping("member/member")
public class MemberController {
    @Autowired
    private MemberService memberService;

    @Autowired
    private CouponFeignService couponFeignService;


    @PostMapping("/oauth/log")
    public R oauthLogin(@RequestBody SocialUserVo vo) throws Exception {

        MemberEntity memberEntity = memberService.login(vo);
        if (memberEntity != null){
            return R.ok().setData(memberEntity);
        } else {
            return R.error(BizCodeEnums.LOGINACCT_PASSWORD_INVAILD_EXCEPTION.getCode(), BizCodeEnums.LOGINACCT_PASSWORD_INVAILD_EXCEPTION.getMsg());
        }
    }


    @PostMapping("/log")
    public R login(@RequestBody MemberLoginVo vo){

        MemberEntity memberEntity = memberService.login(vo);
        if (memberEntity != null){
            return R.ok().setData(memberEntity);
        } else {
            return R.error(BizCodeEnums.LOGINACCT_PASSWORD_INVAILD_EXCEPTION.getCode(), BizCodeEnums.LOGINACCT_PASSWORD_INVAILD_EXCEPTION.getMsg());
        }
    }


    @PostMapping("/regist")
    public R register(@RequestBody MemberRegistVo vo){

        try {
            memberService.register(vo);
        } catch (PhoneExistException e) {
            R.error(BizCodeEnums.PHONE_EXIST_EXCEPTION.getCode(), BizCodeEnums.PHONE_EXIST_EXCEPTION.getMsg());
        } catch (UserNameExistException e){
            R.error(BizCodeEnums.USERNAME_EXIST_EXCEPTION.getCode(), BizCodeEnums.USERNAME_EXIST_EXCEPTION.getMsg());
        }
        return R.ok();

    }


    @RequestMapping("/coupons")
    public R coupons(){
        MemberEntity memberEntity = new MemberEntity();
        memberEntity.setNickname("张三");

        R r = couponFeignService.memberCoupon();
        return R.ok().put("member", memberEntity).put("coupons", r.get("coupons"));
    }


    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("member:member:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = memberService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("member:member:info")
    public R info(@PathVariable("id") Long id){
		MemberEntity member = memberService.getById(id);

        return R.ok().put("member", member);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("member:member:save")
    public R save(@RequestBody MemberEntity member){
		memberService.save(member);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("member:member:update")
    public R update(@RequestBody MemberEntity member){
		memberService.updateById(member);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("member:member:delete")
    public R delete(@RequestBody Long[] ids){
		memberService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
