package com.qzq.gulimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qzq.common.utils.PageUtils;
import com.qzq.gulimall.member.entity.MemberLoginLogEntity;

import java.util.Map;

/**
 * 会员登录记录
 *
 * @author quzongqian
 * @email 2959720739@qq.com
 * @date 2023-03-20 15:22:45
 */
public interface MemberLoginLogService extends IService<MemberLoginLogEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

