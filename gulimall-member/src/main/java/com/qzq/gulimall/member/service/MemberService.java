package com.qzq.gulimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qzq.common.utils.PageUtils;
import com.qzq.gulimall.member.Exception.PhoneExistException;
import com.qzq.gulimall.member.Exception.UserNameExistException;
import com.qzq.gulimall.member.entity.MemberEntity;
import com.qzq.gulimall.member.vo.MemberLoginVo;
import com.qzq.gulimall.member.vo.MemberRegistVo;
import com.qzq.gulimall.member.vo.SocialUserVo;

import java.util.Map;

/**
 * 会员
 *
 * @author quzongqian
 * @email 2959720739@qq.com
 * @date 2023-03-20 15:22:45
 */
public interface MemberService extends IService<MemberEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void register(MemberRegistVo vo);

    void checkPhoneUnique(String phone) throws PhoneExistException;

    void checkUserNameUnique(String userName) throws UserNameExistException;

    MemberEntity login(MemberLoginVo vo);

    MemberEntity login(SocialUserVo vo) throws Exception;
}

