package com.qzq.gulimall.authserver.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.qzq.common.constant.AuthServerConstant;
import com.qzq.common.utils.R;
import com.qzq.gulimall.authserver.feign.MemberServiceFeign;
import com.qzq.gulimall.authserver.utils.HttpUtils;
import com.qzq.common.vo.MemberRespVo;
import com.qzq.gulimall.authserver.vo.SocialUserVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

@Controller
@Slf4j
public class OAuth2Controller {

    @Resource
    private MemberServiceFeign memberServiceFeign;

    @GetMapping("/oauth2.0/gitee/success")
    public String giteeGetAccessTockenToRegist(@RequestParam("code") String code, HttpSession session) throws Exception {

        Map<String, String> map = new HashMap<>();
        map.put("grant_type", "authorization_code");
        map.put("code", code);
        map.put("client_id", "545e25ab300d861829b9756b364aaf6eeb5820f257c52b890ee0b717a6c6908c");
        map.put("redirect_uri", "http://auth.gulimall.com/oauth2.0/gitee/success");
        map.put("client_secret", "fec03adecc4b050eea3d00ef7b0bf3a2759acc9b4ca566b7b85e509dbc914ded");


        HttpResponse response = HttpUtils.doPost("https://gitee.com", "/oauth/token",
                "post", new HashMap<String, String>(), map, new HashMap<String, String>());

        if (response.getStatusLine().getStatusCode() == 200){

            //生成实体
            String s = EntityUtils.toString(response.getEntity());
            SocialUserVo socialUserVo = JSON.parseObject(s, SocialUserVo.class);

            //远程调用member来查询登录用户或者新建用户
            R r = memberServiceFeign.oauthLogin(socialUserVo);
            if (r.getCode() == 0){
                MemberRespVo data = r.getData("data" ,new TypeReference<MemberRespVo>() {});

                log.info("新建或者查找的用户信息为：{}", data);

                //TODO 作用域问题 解决子域共享问题
                //TODO 使用json形式来序列化数据进入redis

                session.setAttribute(AuthServerConstant.LOGIN_USER, data);

                return "redirect:http://gulimall.com";

            } else {
                return "redirect:http://gulimall.com/login.gtml";
            }

        } else {
            return "redirect:http://gulimall.com/login.gtml";
        }


    }

}
