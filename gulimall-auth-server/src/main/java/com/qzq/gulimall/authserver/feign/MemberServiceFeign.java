package com.qzq.gulimall.authserver.feign;

import com.qzq.common.utils.R;
import com.qzq.gulimall.authserver.vo.SocialUserVo;
import com.qzq.gulimall.authserver.vo.UserLoginVo;
import com.qzq.gulimall.authserver.vo.UserRegistVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("gulimall-member")
public interface MemberServiceFeign {

    @PostMapping("/member/member/regist")
    R register(@RequestBody UserRegistVo vo);

    @PostMapping("/member/member/log")
    R login(@RequestBody UserLoginVo vo);

    @PostMapping("/member/member/oauth/log")
    R oauthLogin(@RequestBody SocialUserVo vo);

}
