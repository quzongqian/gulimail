package com.qzq.gulimall.authserver;

import com.qzq.common.constant.AuthServerConstant;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@SpringBootTest
@RunWith(SpringRunner.class)
public class GulimallAuthServerApplicationTests {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Test
    public void contextLoads() {
        //stringRedisTemplate.opsForValue().set(AuthServerConstant.SMS_CODE_CACHE_PREFIX + "15097719253",  "assd1", 5, TimeUnit.SECONDS);
        stringRedisTemplate.opsForValue().set("k2", "v2", 5, TimeUnit.SECONDS);
    }

}
