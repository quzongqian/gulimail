package com.qzq.gulimall.sso.server.controller;

import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.UUID;

@Controller
public class LoginController {

    @Resource
    private StringRedisTemplate stringRedisTemplate;


    @GetMapping("/login.html")
    public String login(@RequestParam("redirect_url") String url,
                        Model model){

        model.addAttribute("url", url);

        return "login";
    }


    @PostMapping("/doLogin")
    public String doLogin(@RequestParam("username") String username,
                          @RequestParam("password") String password,
                          @RequestParam("url") String url){

        if (!StringUtils.isEmpty(username) && !StringUtils.isEmpty(password)){

            String uuid = UUID.randomUUID().toString().replace("-", "");
            stringRedisTemplate.opsForValue().set(uuid, username);


            return "redirect:" + url + "?token=" + uuid;
        }

        return "login";
    }

}
